#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 推定結果とdensityを元に分類
###########################
import sys
import time
import argparse
import glob
import features as F

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm  as cm
import os
import shutil

labels = {
    "Interest": (
        "興味なし",
        "ほとんど興味なし",
        "少し興味深い",
        "興味深い"
    ),
    "Polite":(
        "読まなかった",
        "流し読み",
        "どちらか流し読み",
        "どちらか丁寧",
        "丁寧"
    ),
    "Concentration" :(
        "集中してない",
        "ほとんど集中してない",
        "少し集中できた",
        "集中できた"
    ),
    "Know":(
        "知らない",
        "知っていた"
    ),
    "hasKnow":(
        "知りたいことはない",
        "もっと知りたい"
    ),
    "Difficult":(
        "簡単",
        "どちらとも言えない",
        "難しい"
    ),
    "pre":(
        "知りたくない",
        "あまり知りたくない",
        "少し知りたい",
        "知りたい"
    ),
    "mode":(
        "なし",
        "時間制限"
    )
}

def merge_result(results,densitys,ankert):
    temp = pd.merge(results,densitys,on='path')
    return pd.merge(temp,ankert,on='path')

def concat_resultTodensity(results,densitys):
    return pd.merge(results,densitys,on='path')

def copyGraph(_out,_article,bins):
    #import ipdb;ipdb.set_trace()
    binpath = bins[_article.density <= bins.bin].bin.values[0]
    _path = _out + str(binpath) + "/" + labels["Interest"][_article.ans] + "/" + labels["Interest"][_article.pred]
    if not os.path.exists(_path):
        os.makedirs(_path)
    shutil.copy2(_article.path.replace("_dom.csv",".png"),_path + "/")
    #import ipdb;ipdb.set_trace()
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--result',type=str, default="/",
                        help='推定結果csv')
    parser.add_argument('--density',type=str, default="/",
                        help='閾値csv')
    parser.add_argument('--ankert',type=str, default="",
                        help='アンケートcsv')
    parser.add_argument('--out',type=str, default="view/",
                        help='出力ディレクトリ')

    args = parser.parse_args()

    results = pd.read_csv(args.result,names=["target","path","ans","pred"])
    densitys = pd.read_csv(args.density)
    bins = pd.read_csv(args.density.replace("density.csv","bins.csv"),names=["bin"])
    if not os.path.exists(args.out):
        print("create view directory")
        os.makedirs(args.out)
    
    """
    結果とdensityのマージ
    """
    datas =  concat_resultTodensity(results,densitys)
    datas.to_csv(args.out + "temp.csv")
    if args.ankert != "":
        ankert = pd.read_csv(args.ankert)
        merge_result(results,densitys,ankert).to_csv(args.out + "temp2.csv")

    for i,article in datas.iterrows():
        sys.stdout.write("\r%d" % i)
        sys.stdout.flush()
        """
        1記事ごとにdensityごとに，推定結果で振り分け
        """
        copyGraph(args.out,article,bins)
        time.sleep(0.01)





if __name__ == "__main__":
    main()
    pass