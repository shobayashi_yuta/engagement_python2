
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as cm
from matplotlib.backends.backend_pdf import PdfPages
#import cupy

class Gaze:
    def __init__(self,path):
        self.path = path
        self.gazes = self.read_gazes(path)
        self.url = self.gazes[self.gazes['x'] == 'url']# urlの位置を記憶
        self.dic = {}

    def read_gazes(self,path):
        '''
        path:attention.csvがあるディレクトリ
        '''
        df = pd.read_csv(path + "/attention.csv",names=('x','y','time','word','type'),encoding="utf-8")
        df = df.append(pd.DataFrame(["url"],columns=['x']),ignore_index=True)
        return df.ix[:,['x','y','time','word','type']]
 
    def pickPageDataset(self,webpages):
        '''
        Web記事ごとに視線情報をまとめる
        args: 収録したWeb記事のpage番号とURL
        return: 辞書型({URL:[Web記事の通し番号，視線情報，記事中の言語とその位置]})
        '''

        # 記事閲覧時の視線行動のインデックス範囲
        for _i,_webpage in webpages.iterrows():
            PageRange = [self.url.index[np.where(self.url["y"] == _webpage["url"])[0][0]]+1,self.url.index[np.where(self.url["y"] == _webpage["url"])[0][0]+1]]
            gaze =self.gazes[PageRange[0]:PageRange[1]].astype({'x':int,'y':int,'time': np.double})
            #words = self.wordsplit(words)
            #print(words.columns)
            self.dic[_webpage["url"]] = gaze
    
### 特徴量のメソッド ###

def gazeRetentionCount(gaze):
    '''
    注視回数
    '''
    retentionCount = 1
    if len(gaze) > 1:
        cur = gaze.index[0]
        for _i in gaze.index[1:]:
            if gaze["time"][_i] - gaze["time"][cur] > 20:
                retentionCount+=1
                cur = _i
            else:
                cur = _i
    elif len(gaze) == 1:
        #import ipdb;ipdb.set_trace()
        retentionCount = 1
    else:
        retentionCount = 0
    
    return retentionCount

def gazeInword(_gaze,_word,overheight,overwidth):
    '''
    単語範囲
    '''
    data = _gaze[(_gaze["x"] >= (_word["left"]-overwidth)) & 
                (_gaze["x"] <= (_word["right"]+overwidth)) &
                    (_gaze["y"] >= (_word["top"]-overheight)) &
                    (_gaze["y"] <= (_word["bottom"]+overheight))]
    return data

def wordhist(_df,_words,_overheight,_overwidth,gpu=-1):
    '''
        単語範囲に対する視線の入り・出る
        [左入，下入，右入，上入，左出，下出，右出，上出]
        正規化ずみ
    '''
    wh = []
    maxCount = 0
    if gpu < 0:
        device=np
    else:
        device=cupy

    for _i,_w in _words.iterrows():
        
        #import ipdb;ipdb.set_trace()
        _wh = np.zeros(8)
        _wh2 = np.zeros(8)
        cur_f = _df.values[0][:2]
    	#print(_df[0:2])
        _top = _w["top"] - _overheight
        _bottom = _w["bottom"] + _overheight
        _right = _w["right"] + _overwidth
        _left = _w["left"] - _overwidth
        for _,_f in _df[1:].iterrows():
            #print(cur_f,_f.values[:2])

            # 右判定
            _index=check_cross(cur_f,_f.values[:2],[_right,_top],[_right,_bottom],_right,6,2,device)
            if _index is not None:
                _wh[_index] += 1 

            # 左判定
            _index=check_cross(cur_f,_f.values[:2],[_left,_top],[_left,_bottom],_right,0,4,device)
            if _index is not None:
                _wh[_index] += 1 

            # 上判定
            _index=check_cross(cur_f,_f.values[:2],[_right,_top],[_left,_top],_top,3,7,device)
            if _index is not None:
                _wh[_index] += 1 

            # 下判定
            _index=check_cross(cur_f,_f.values[:2],[_right,_bottom],[_left,_bottom],_top,5,1,device)
            if _index is not None:
                _wh[_index] += 1 


            cur_f = _f.values[:2]
        wh.append(_wh)
        if maxCount < _wh.max():
            maxCount = _wh.max()
    
    wh = [ w / maxCount for w in wh]
    #ipdb.set_trace()
    return wh

def CalcDistanceToWord(pos_cur,pos,_index1,_index2):
    '''
    単語範囲との距離
    どれくらい遠くから見に来たか、どれくらい遠くに見に行ったか
    '''
    return False    

def check_cross(pos_cur,pos,p1,p2,bar,_in,_out,device):
    if crossjudge_cross(pos_cur,pos,p1,p2,device):
        if pos[1]- pos_cur[1] > 0:# 左から右or上から下
            if pos[1] != bar:
                return _in
        elif pos[1] - pos_cur[1] < 0:# 右から左or下から上
            if pos_cur[1] != bar:
                return _out
    return None

def crossjudge_cross(p1,p2,p3,p4,device):
    if len(p3) == 1:
        ab = device.array([p2[0]-p1[0],p2[1]-p1[1]])
        ac = device.array([p3[0]-p1[0],p3[1]-p1[1]])
        cd = device.array([p4[0]-p3[0],p4[1]-p3[1]])
        C0 = device.cross(ab,cd) # 平行かを判定
        if C0 == 0: # 平行
            return False
        Ca = device.cross(ac,ab)
        Cb = device.cross(ac,cd)
    
        #内分比
        R0 = Ca / C0
        R1 = Cb / C0
        if R0 >= 0 and R0 <=1 and R1 >= 0 and R1 <= 1:
            return True
        return False
    else:
        ab = device.array([p2[:,0]-p1[:,0],p2[:,1]-p1[:,1]]).T
        ac = device.array([p3[:,0]-p1[:,0],p3[:,1]-p1[:,1]]).T
        cd = device.array([p4[:,0]-p3[:,0],p4[:,1]-p3[:,1]]).T
        C0 = device.cross(ab,cd) # 平行かを判定
        #checklist = np.where(C0 == 0,False,True)
        #import ipdb;ipdb.set_trace()
        Ca = device.cross(ac,ab)
        Cb = device.cross(ac,cd)    
        #内分比
        R0 = Ca / C0
        R1 = Cb / C0
        checklist = np.where((C0!=0) & (R0 >=0) & (R0 <=1) & (R1 >= 0) & (R1 <= 1) ,True,False)
        #if R0 >= 0 and R0 <=1 and R1 >= 0 and R1 <= 1:
        #    return True
        #return False
        return checklist





class Graph:
    def __init__(self,path,_page):
        self.pp = PdfPages(path + "/%d_article.pdf" % _page)
        self.fig = plt.figure()
        self.ax = plt.axes()
        

    def graph_articlePerCell(self,words,_gazes,ans,gpu):
        '''
        記事を簡単に分割して表示
        '''
        
        # ミニ記事のグリッド
        #xxx = np.arange(150,words["right"].max()+400,400)
        xxx = np.array([150,900])
        yyy = np.arange(150,words["bottom"].max()+300,300)
        xxx,yyy = np.meshgrid(xxx,yyy)
        for x in range(xxx.shape[0]-1): # 左上の格子点index
            for y in range(xxx.shape[1]-1):
                self.fig = plt.figure()
                self.ax = plt.axes()
                #print(xxx[x,y],yyy[x,y])
                self.makeminiGraph(words,_gazes,ans,(xxx[x,y],yyy[x,y]),(xxx[x,y+1],yyy[x,y+1]),(xxx[x+1,y],yyy[x+1,y]),(xxx[x+1,y+1],yyy[x+1,y+1]))
                #plt.show()
                plt.gca().invert_yaxis()
                plt.title("x=" + str(xxx[x,y]) + " y=" + str(yyy[x,y]))
                self.pp.savefig(self.fig)
                plt.close(self.fig)
                
        self.pp.close()
        plt.close()


    def makeminiGraph(self,words,_gazes,ans,p1,p2,p3,p4):
        '''
        記事の図示
        arg: 左上，右上，左下，右下
        '''        
        #import ipdb;ipdb.set_trace()
        self.ax.set_ylim([p1[1],p4[1]])
        self.ax.set_xlim([p1[0],p4[0]])
        xmin, xmax = p1[0],p4[0]
        ymin, ymax = p1[1],p4[1]
        self.printWord(words,ans,xmin,xmax,ymin,ymax)
        self.printgaze(_gazes,xmin,xmax,ymin,ymax)
        
        
        #return fig


    def printWord(self,_words,ans,xmin,xmax,ymin,ymax):
        #import ipdb;ipdb.set_trace()
        for _i,_w in _words.iterrows():
            #print(_w["word"])
            x = np.arange(_w["left"],_w["right"])
            yTop = [_w["top"]] * len(x)
            yBottom = [_w["bottom"]] * len(x)
            self.ax.fill_between(x,yTop,yBottom,facecolor="white",edgecolor='g')            
            if _w["word"] != _w["word"]:
                continue
            elif type(ans) != float and _w["word"] in ans.split('|'):
                wordcolor = 'r'
            else:
                wordcolor = 'k'
            if ((_w["left"] +_w["right"])/2 >= xmin) & ((_w["left"] +_w["right"])/2 <= xmax) & ((_w["top"]+_w["bottom"])/2 >= ymin) & ((_w["top"]+_w["bottom"])/2 <= ymax):
                self.ax.text((_w["left"] +_w["right"])/2,(_w["top"]+_w["bottom"])/2,_w["word"],horizontalalignment='center',verticalalignment='center',fontsize=5,color=wordcolor)
        

    def printgaze(self,_gazes,xmin,xmax,ymin,ymax):
        #init_x = _gazes["x"].values[0]
        #init_y = _gazes["y"].values[0]
        
        _gazes["sy"] = _gazes["y"].shift()
        _gazes["sx"] = _gazes["x"].shift()
        _gazes["dt"] = _gazes["time"] - _gazes["time"].values[0]
        _gazes = _gazes[1:]
        tmp_gazes = _gazes[(crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmin,ymin]]*len(_gazes)),np.array([[xmin,ymax]]*len(_gazes)),np) | crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmax,ymin]]*len(_gazes)),np.array([[xmax,ymax]]*len(_gazes)),np) | crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmin,ymax]]*len(_gazes)),np.array([[xmax,ymax]]*len(_gazes)),np) | crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmax,ymin]]*len(_gazes)),np.array([[xmin,ymin]]*len(_gazes)),np)) | (((_gazes["sx"] >= xmin) & (_gazes["sx"] <= xmax) & (_gazes["sy"] >= ymin) & (_gazes["sy"] <= ymax)) | ((_gazes["x"] >= xmin) & (_gazes["x"] <= xmax) & (_gazes["y"] >= ymin) & (_gazes["y"] <= ymax)))]
        #import ipdb;ipdb.set_trace()
        tmp_gazes["dt"] = tmp_gazes["time"] - tmp_gazes["time"].values[0] # ミニ記事ごとに視線の時系列を示す
        self.ax.quiver(tmp_gazes["sx"].values,tmp_gazes["sy"].values,tmp_gazes["x"].values-tmp_gazes["sx"].values,tmp_gazes["y"].values-tmp_gazes["sy"].values,angles='xy',scale_units='xy',scale=1,width=0.005,color=cm.cool(tmp_gazes["dt"].values/tmp_gazes["dt"].values.max()))  
        """
        for x,y,t in zip(_gazes["x"].values[1:],_gazes["y"].values[1:],_gazes["time"].values[1:] - _gazes["time"].values[0]):
            #print(x,y,t,t/(_gazes["time"].values - _gazes["time"].values[0]).max())
            #ax.plot(np.array([init_x,x]),np.array([init_y,y]),color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
            #ax.plot(np.array([init_x,x]),np.array([init_y,y]),color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
            #ax.quiver(init_x,init_y,x-init_x,y-init_y,angles='xy',scale_units='xy',scale=1,width=0.0015,color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
            #if c == (10 - 1 + len(ans.split("|"))):
            #ax.quiver(init_x,init_y,x-init_x,y-init_y,angles='xy',scale_units='xy',scale=1,width=0.0025,color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
            #else:
                #if ((init_x >= xmin) & (init_x <= xmax) & (init_y >= ymin) & (init_y <= ymax)) | ((x >= xmin) & (x <= xmax) & (y >= ymin) & (y <= ymax)):
            if ((crossjudge_cross([init_x,init_y],[x,y],[xmin,ymin],[xmin,ymax],np) | crossjudge_cross([init_x,init_y],[x,y],[xmax,ymin],[xmax,ymax],np) | crossjudge_cross([init_x,init_y],[x,y],[xmin,ymax],[xmax,ymax],np) | crossjudge_cross([init_x,init_y],[x,y],[xmax,ymin],[xmin,ymin],np)) | (((init_x >= xmin) & (init_x <= xmax) & (init_y >= ymin) & (init_y <= ymax)) | ((x >= xmin) & (x <= xmax) & (y >= ymin) & (y <= ymax)))) :
                self.ax.quiver(init_x,init_y,x-init_x,y-init_y,angles='xy',scale_units='xy',scale=1,width=0.005,color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))  
            init_x = x
            init_y = y
        """

def graph_article(_words,_gazes,ans,_page,path,gpu):
    
    #print(ans,type(ans))
    
    if type(ans) != float and type(ans) != np.float64 :
        #print(ans,type(ans))
        pp = PdfPages(path + "/%d_article.pdf" % _page)
        for c in range(10 + len(ans.split("|"))):
            #print(ans)
            #print(_words[_words["word"] == ans.split('|')[c]])
            if c < len(ans.split("|")) and len(_words[_words["word"] == ans.split('|')[c]]) == 0 :
                continue
            fig = plt.figure()
            ax = plt.axes()

            if c < len(ans.split('|')):
                
                #plt.title("page:%s expectation:%s key:%s,main:%s" % (_page,_ankert[0],ans,ans.split('|')[c]))
                
                w = _words[_words["word"] == ans.split('|')[c]][-1:]
                #print(w)
                ax.set_ylim([w["top"].values[0]-40,w["bottom"].values[0]+40])
                ax.set_xlim([w["left"].values[0]-40,w["right"].values[0]+40])
                
            elif c < (10 - 1 + len(ans.split("|"))):
                w = _words.sample(frac=1,random_state=0)[c:c+1]
                #plt.title("page:%s expectation:%s key:%s,main:%s" % (_page,_ankert[0],ans,w["word"].values[0]))
                ax.set_ylim([w["top"].values[0]-40,w["bottom"].values[0]+40])
                ax.set_xlim([w["left"].values[0]-40,w["right"].values[0]+40])
                
            #else:
                #plt.title("page:%s expectation:%s key:%s" % (_page,_ankert[0],ans))
            xmin, xmax = plt.xlim()    
            ymin, ymax = plt.ylim()
            #print(c,xmin,xmax,ymin,ymax)

            #plt.gca().add_patch( plt.Rectangle(xy=[0.25, 0.25], width=0.5, height=0.5) )
            #plt.show()
            #print(ans)
            for _i,_w in _words.iterrows():
                #print(_w["word"],_w["right"]-_w["left"],_w["bottom"]-_w["top"])
                x = np.arange(_w["left"],_w["right"])
                yTop = [_w["top"]] * len(x)
                yBottom = [_w["bottom"]] * len(x)
                ax.fill_between(x,yTop,yBottom,facecolor="white",edgecolor='g')
                
                if _w["word"] != _w["word"]:
                    continue
                elif type(ans) != float and _w["word"] in ans.split('|'):
                    wordcolor = 'r'
                else:
                    wordcolor = 'k'
                
                if ((_w["left"] +_w["right"])/2 >= xmin) & ((_w["left"] +_w["right"])/2 <= xmax) & ((_w["top"]+_w["bottom"])/2 >= ymin) & ((_w["top"]+_w["bottom"])/2 <= ymax):
                    ax.text((_w["left"] +_w["right"])/2,(_w["top"]+_w["bottom"])/2,_w["word"],horizontalalignment='center',verticalalignment='center',fontsize=15,color=wordcolor)
                elif c == (10 - 1 + len(ans.split("|"))):
                    ax.text((_w["left"] +_w["right"])/2,(_w["top"]+_w["bottom"])/2,_w["word"],horizontalalignment='center',verticalalignment='center',fontsize=5,color=wordcolor)
            #print(_gazes)
            init_x = _gazes["x"].values[0]
            init_y = _gazes["y"].values[0]
            for x,y,t in zip(_gazes["x"].values[1:],_gazes["y"].values[1:],_gazes["time"].values[1:] - _gazes["time"].values[0]):
                #print(x,y,t,t/(_gazes["time"].values - _gazes["time"].values[0]).max())
                #ax.plot(np.array([init_x,x]),np.array([init_y,y]),color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
                #ax.plot(np.array([init_x,x]),np.array([init_y,y]),color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
                #ax.quiver(init_x,init_y,x-init_x,y-init_y,angles='xy',scale_units='xy',scale=1,width=0.0015,color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
                if c == (10 - 1 + len(ans.split("|"))):
                    ax.quiver(init_x,init_y,x-init_x,y-init_y,angles='xy',scale_units='xy',scale=1,width=0.0025,color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))
                else:
                    #if ((init_x >= xmin) & (init_x <= xmax) & (init_y >= ymin) & (init_y <= ymax)) | ((x >= xmin) & (x <= xmax) & (y >= ymin) & (y <= ymax)):
                    if ((crossjudge_cross([init_x,init_y],[x,y],[xmin,ymin],[xmin,ymax],np) | crossjudge_cross([init_x,init_y],[x,y],[xmax,ymin],[xmax,ymax],np) | crossjudge_cross([init_x,init_y],[x,y],[xmin,ymax],[xmax,ymax],np) | crossjudge_cross([init_x,init_y],[x,y],[xmax,ymin],[xmin,ymin],np)) | (((init_x >= xmin) & (init_x <= xmax) & (init_y >= ymin) & (init_y <= ymax)) | ((x >= xmin) & (x <= xmax) & (y >= ymin) & (y <= ymax)))) :
                        ax.quiver(init_x,init_y,x-init_x,y-init_y,angles='xy',scale_units='xy',scale=1,width=0.01,color=cm.cool(t/(_gazes["time"].values - _gazes["time"].values[0]).max()))  
                init_x = x
                init_y = y
            #ax.scatter(_gazes["x"].values,_gazes["y"].values,c=_gazes["time"].values-_gazes["time"].values[0],marker='o',cmap='cool',s=2)
            plt.gca().invert_yaxis()
            plt.xlabel("x軸",fontsize=9)
            plt.ylabel("y軸",fontsize=9)
                
            #plt.show()
            #sys.exit()
            pp.savefig(fig)
            plt.close(fig)
        pp.close()
        plt.close()
