#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# アンケート結果の条件に応じた記事を閲覧
###########################

import argparse
import glob
import features as F

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm  as cm
import os
import shutil
import itertools

labels = {
    "Interest": (
        "興味なし",
        "ほとんど興味なし",
        "少し興味深い",
        "興味深い"
    ),
    "Polite":(
        "読まなかった",
        "流し読み",
        "どちらか流し読み",
        "どちらか丁寧",
        "丁寧"
    ),
    "Concentration" :(
        "集中してない",
        "ほとんど集中してない",
        "少し集中できた",
        "集中できた"
    ),
    "Know":(
        "知らない",
        "知っていた"
    ),
    "hasKnow":(
        "知りたいことはない",
        "もっと知りたい"
    ),
    "Difficult":(
        "簡単",
        "どちらとも言えない",
        "難しい"
    ),
    "pre":(
        "知りたくない",
        "あまり知りたくない",
        "少し知りたい",
        "知りたい"
    ),
    "mode":(
        "なし",
        "時間制限"
    )
}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--result',type=str, default="/",
                        help='推定結果csv')
    args = parser.parse_args()

    results = pd.read_csv(args.result,names=["target","path","ans","pred"])
    directory = "analysis/view_result/"
    #import ipdb;ipdb.set_trace()
    for i,j in itertools.product(labels["Interest"],labels["Interest"]):
        print(i,j,labels["Interest"].index(i),labels["Interest"].index(j))
        os.makedirs(directory + i + "/" + j)
        for _,_result in results[(results["ans"] == labels["Interest"].index(i)) & (results["pred"] == labels["Interest"].index(j)) ].iterrows(): 
            shutil.copy2(_result["path"].replace("_dom.csv",".png"),directory + i + "/" + j)
    """
    if not os.path.exists("analysis/view/" + x_col):
        os.mkdir("analysis/view/" + x_col)
    if not os.path.exists("analysis/view/" + x_col +"/" + y_col):
        os.mkdir("analysis/view/" + x_col +"/" + y_col)
    directory = "analysis/view/" + x_col +"/" + y_col

    for x in range(len(labels[x_col])):
        if not os.path.exists(directory + "/" + labels[x_col][x]):
            os.mkdir(directory + "/" + labels[x_col][x]) 
        for y in range(len(labels[y_col])):
            if not os.path.exists(directory + "/" + labels[x_col][x] + "/" + labels[y_col][y]):
                os.mkdir(directory + "/" + labels[x_col][x] + "/" + labels[y_col][y])
            targets =  ankerts[(ankerts[x_col] == x) & (ankerts[y_col] == y)]
            for i,target in  targets.iterrows():
                path = target["path"]
                mode = target["mode"]
                print(path,mode)
                #import ipdb;ipdb.set_trace()
                if not os.path.exists(directory + "/" + labels[x_col][x] + "/" + labels[y_col][y] + "/" + str(mode)):
                    os.mkdir(directory + "/" + labels[x_col][x] + "/" + labels[y_col][y]  + "/" + str(mode))
                #import ipdb;ipdb.set_trace()
                shutil.copy2(path.replace("_dom.csv",".png"),directory + "/" + labels[x_col][x] + "/" + labels[y_col][y] + "/" + str(mode))
    """



if __name__ == "__main__":
    main()
    pass