#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 結果に関すること
###########################

import sys
import pandas as pd
import result as R
import glob
import features as F
import os

class Result:
    def __init__(self):
        self.results = None

    def merge(self,result):
        if self.results is None:
            self.results = result
        else:
            self.results = pd.merge(self.results,result,on=["path","label"],how="outer")    
        
        return 0
    
    def mergeAnk(self,ankert):
        import ipdb;ipdb.set_trace()
        self.results = pd.merge(self.results,ankert,on=["path"])


def merge_Ankert(paths):
    ankerts = pd.DataFrame([])
    for articlepath in paths:
        """
        記事ごとに処理
        """
        print(articlepath)
        _dir = "/".join(articlepath.split("/")[:-1])
        _id = articlepath.split("/")[-1].replace("_dom.csv","")
        #if os.path.exists("cache/" + articlepath.replace("/","_")+ '.pickle'):
        #    continue
        # キャッシュを考慮してデータを読み込む
        #import ipdb;ipdb.set_trace()
        df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert =  F.readdataforCache(articlepath,_dir,_id)
        _ankert.path = "/".join([ i for i in  _ankert.path.split("/") if not i == ""])  + "_dom.csv" 
        
        """
        preankerts =  F.read_preankert(_dir)
        topic =   [True if  (int(_id) -n_page) in _pre["id"].values else False for _pre in preankerts][0] # 興味のあるトピックの記事かどうか
        
        if topic:
            #興味のあるトピックの記事
            preankerts = preankerts[0]
        else:
            #興味のないトピックの記事
            preankerts = preankerts[1]
        
        #print(_id,n_page)
        preankert = preankerts[preankerts["id"]  == (int(_id) -n_page)]# 事前の印象
        _ankert = F.read_ankert(articlepath.replace("_dom","_ankert").replace(_id,str(int(_id)-n_page)))# アンケート結果
        """
        #import ipdb;ipdb.set_trace()
        ankerts = ankerts.append(_ankert,ignore_index=True)
    #import ipdb;ipdb.set_trace()
    ankerts = ankerts[["path","genre","title","pre","Interest","Polite","Know",
                        "hasKnow","Concentration","Difficult","mode"]]
    return ankerts

ankerts = merge_Ankert(glob.glob(sys.argv[1] + "*/*/*/*_dom.csv"))

result = Result()

for path in sys.argv[2:]:
    _result = R.read_result(path)
    print(len(_result))
    _result.columns = ["target","path","label",path.replace("/","_")]
    result.merge(_result)
#import ipdb;ipdb.set_trace()
result.mergeAnk(ankerts)

result.results.to_csv("temp.csv")