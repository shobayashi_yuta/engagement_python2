#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 推定器
###########################

import argparse
import glob
import features as Features
import copy
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import result
import csv
import density
import view_perAnkert as vAnkert

import chainer
import chainer.functions as F
from chainer import function
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer.backends import cuda
from chainer import reporter as reporter_module
from multiprocessing import Pool

def wrapper(args):
    args,target,train,test = args
    return loop(args,target,train,test)

# Network definition
class MLP(chainer.Chain):

    def __init__(self, n_units, n_out):
        use_dropout = 0.50
        hidden_size =  n_units
        n_layers = 1
        super(MLP, self).__init__()
        with self.init_scope():
            # the size of the inputs to each layer will be inferred
            self.bilstm1=L.NStepLSTM(n_layers=n_layers, in_size=6,
                    out_size=hidden_size, dropout=use_dropout)
            self.bilstm2=L.NStepLSTM(n_layers=n_layers, in_size=3,
                    out_size=hidden_size, dropout=use_dropout)
            self.bilstm3=L.NStepLSTM(n_layers=n_layers, in_size=14,
                    out_size=hidden_size, dropout=use_dropout)
            self.bilstm4=L.NStepBiLSTM(n_layers=n_layers, in_size=6,
                    out_size=hidden_size, dropout=use_dropout)

            self.l5 = L.Linear(None,1) 

            self.fc1 = L.Linear(None, hidden_size)
            #self.fc2 = L.Linear(None, hidden_size)
            #self.fc3 = L.Linear(None, int(hidden_size / 2))
            #self.fc4 = L.Linear(None, int(hidden_size / 4))
            self.fc = L.Linear(None, n_out)  # n_units -> n_out
            self.bn = L.BatchNormalization(hidden_size*2*4 + 1)
            
    def forward(self, x):
        
        #x = self.split(x)
        
        hy, _, _ = self.bilstm1(None, None, x[0])
        #hy, _= self.bigru1( None, x[0])
        #y1 = F.tanh(self.bn(F.concat(hy)))
        #y1 = F.tanh(F.concat(hy))
        y1 = F.concat(hy)
        #import ipdb;ipdb.set_trace()
        #y1 = F.tanh(F.dropout(F.concat(hy),ratio=0.0))
        
        hy2, _, _ = self.bilstm2(None, None, x[1])
        #y2 = F.tanh(self.bn(F.concat(hy2)))
        #y2 = F.tanh(F.concat(hy2))
        y2 = F.concat(hy2)
        #y2 = F.tanh(F.dropout(F.concat(hy2),ratio=0.0))
        hy3, _, _ = self.bilstm3(None, None, x[2])
        #y3 = F.tanh(self.bn(F.concat(hy3)))
        #y3 = F.tanh(F.concat(hy3))
        y3 = F.concat(hy3)
        #y3 = F.tanh(F.dropout(F.concat(hy3),ratio=0.0))
        
        hy4, _, _ = self.bilstm4(None, None, x[3])
        #hy4, _ = self.bigru4(None, x[3])
        #y4 = F.tanh(self.bn(F.concat(hy4)))
        #y4 = F.tanh(F.concat(hy4))
        y4 = F.concat(hy4)
        #y4 = F.tanh(F.dropout(F.concat(hy4),ratio=0.0))
        
        #y5 = F.tanh(self.bn(self.l5(F.concat(x[4],axis=0))))
        #y5 = F.tanh(F.dropout(self.l5(F.concat(x[4],axis=0)),ratio=0.0))
        #y5 = F.tanh(self.l5(F.concat(x[4],axis=0)))
        y5 = F.concat(x[4],axis=0)
        #y  = F.dropout(F.concat([y1,y2,y3,y4,y5],axis=1),ratio=0.5)
        #import ipdb;ipdb.set_trace()
        #y  = F.tanh(self.bn(F.concat([y1,y2,y3,y4,y5],axis=1)))
        y  = F.tanh(F.concat([y1,y2,y3,y4,y5],axis=1))
        #y  = F.concat([y1,y4,y5],axis=1)
        #h1 =  F.tanh(self.bn64(self.fc1(y)))
        #h1 =  F.tanh(self.fc1(y))
        h1 =  F.tanh(F.dropout(self.fc1(y),ratio=0.5))
        #h3 =  F.tanh(self.bn64(self.fc2(h1)))
        #h2 =  F.tanh(self.fc2(h1))
        #h2 =  F.tanh(F.dropout(self.fc2(h1),ratio=0.25))
        #h3 =  F.tanh(self.bn32(self.fc3(h2)))
        #h3 =  F.tanh(self.fc3(h2))
        #h3 =  F.tanh(F.dropout(self.fc3(h2),ratio=0.25))
        #h4 =  F.tanh(self.bn16(self.fc4(h3)))
        #h4 =  F.tanh(self.fc4(h3))
        #h4 =  F.tanh(F.dropout(self.fc4(h3),ratio=0.25))
        return  self.fc(h1)

class myEvaluator(chainer.training.extensions.Evaluator):
    def evaluate(self):
        
        iterator = self._iterators['main']
        eval_func = self.eval_func or self._targets['main']

        if self.eval_hook:
            self.eval_hook(self)

        if hasattr(iterator, 'reset'):
            iterator.reset()
            it = iterator
        else:
            it = copy.copy(iterator)

        summary = reporter_module.DictSummary()
        pred = None
        ans = None
        if self.device is None:
            xp = np
        elif self.device < 0:
            xp = np
        else:
            xp = cuda.cupy

        for batch in it:
            observation = {}
            with reporter_module.report_scope(observation):
                _batch =  self.converter(batch,device=self.device)
                with function.no_backprop_mode(),\
                    chainer.using_config('train',False):
                    temp = eval_func.predictor(_batch[0]).data
                if pred is None:
                    pred = temp
                    ans = _batch[1]
                else:
                    pred = F.concat([pred,temp],axis=0)
                    ans = xp.concatenate([ans,_batch[1]])
        loss = F.softmax_cross_entropy(pred,ans)
        accuracy = F.accuracy(pred,ans)
        #import ipdb;ipdb.set_trace()
        summary.add({'accuracy': accuracy})
        summary.add({'loss': loss})
        summary.add(observation)
        return summary.compute_mean()
        #return None

def convert(batch,device=-1,label="Interest"):
    datasets = None
    ankerts = None
    
    if device is None:
        xp = np
    elif device < 0:
        xp = np
    else:
        xp = cuda.cupy
    for b in batch:
        dataset,ankert = Features.readfeatureforCache(b)
        dataset[0] = dataset[0][::10]  # 間引く
        if datasets is None:
            datasets = [[chainer.dataset.to_device(device,feature)] for feature in dataset]
            ankerts = ankert
        else:
            for i,feature in enumerate(dataset):
                datasets[i].append(chainer.dataset.to_device(device,feature))
            #datasets.append(dataset)
            ankerts = pd.concat([ankerts,ankert],ignore_index=False)
    #first_elem = batch[0]
    ans = makeOutput(ankerts,label)
    ans = xp.array(ans).astype(xp.int32)
    #temp = F.transpose(np.array(datasets),axes=(1,0))
    return datasets,ans

def makeOutput(label,target):
    labels = label[target].values.astype(int)
    #import ipdb;ipdb.set_trace()
    """
    if target == "pre":
        labels = np.array([int(l[1]["pre"]) for l in label])
    else:
        labels = np.array([int(l[2][target]) for l in label])
    """
    return labels

def net(args,train,test):
    #import ipdb;ipdb.set_trace()
    # Set up a neural network to train
    # Classifier reports softmax cross entropy loss and accuracy at every
    # iteration, which will be used by the PrintReport extension below.
    global xp
    xp  = np
    model = L.Classifier(MLP(args.unit, 4))
    if args.gpu >= 0:
        # Make a specified GPU current
        chainer.backends.cuda.get_device_from_id(args.gpu).use()
        model.to_gpu()  # Copy the model to the GPU
        xp = chainer.backends.cuda.cupy

    # Setup an optimizer
    #optimizer = chainer.optimizers.Adam()
    optimizer = chainer.optimizers.MomentumSGD(lr=0.001)
    #optimizer = chainer.optimizers.Adam(alpha=0.005,beta1=0.9,beta2=0.999,eps=1e-08)
    #optimizer = chainer.optimizers.SGD(lr=0.01)
    optimizer.setup(model)

    # Load the MNIST dataset
    #_train, _test = chainer.datasets.get_mnist()
    #import ipdb;ipdb.set_trace()
    train_iter = chainer.iterators.SerialIterator(train, args.batchsize,repeat=True,shuffle=True)
    #import ipdb;ipdb.set_trace()
    test_iter = chainer.iterators.SerialIterator(test, args.batchsize,
                                                                    repeat=False, shuffle=False)
    # Set up a trainer
    updater = training.updaters.StandardUpdater(
        train_iter, optimizer,converter=convert,device=args.gpu)
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), out=args.out)

    #eval_model = model.copy()
    # Evaluate the model with the test dataset for each epoch
    trainer.extend(myEvaluator(test_iter, model,converter=convert,device=args.gpu))

    # Dump a computational graph from 'loss' variable at the first iteration
    # The "main" refers to the target link of the "main" optimizer.
    trainer.extend(extensions.dump_graph('main/loss'))

    # Take a snapshot for each specified epoch
    frequency = args.epoch if args.frequency == -1 else max(1, args.frequency)
    trainer.extend(extensions.snapshot(), trigger=(frequency, 'epoch'))

    # Write a log of evaluation statistics for each epoch
    trainer.extend(extensions.LogReport())

    # Save two plot images to the result dir
    if args.plot and extensions.PlotReport.available():
        trainer.extend(
            extensions.PlotReport(['main/loss', 
            'loss'
            ],
                                  'epoch', file_name='loss.png'))
        trainer.extend(
            extensions.PlotReport(
                ['main/accuracy', 
                'accuracy'
                ],
                'epoch', file_name='accuracy.png'))

    # Print selected entries of the log to stdout
    # Here "main" refers to the target link of the "main" optimizer again, and
    # "validation" refers to the default name of the Evaluator extension.
    # Entries other than 'epoch' are reported by the Classifier link, called by
    # either the updater or the evaluator.
    trainer.extend(extensions.PrintReport(
        ['epoch', 'main/loss', 'loss',
         'main/accuracy', 
         'accuracy',
          'elapsed_time']))

    # Print a progress bar to stdout
    trainer.extend(extensions.ProgressBar())

    if args.resume:
        # Resume from a snapshot
        chainer.serializers.load_npz(args.resume, trainer)

    # Run the training
    trainer.run()

    test_iter.reset()
    #import ipdb;ipdb.set_trace()
    pred = None
    ans = None
    for batch in  test_iter:
        _batch =  convert(batch,device=args.gpu)
        with function.no_backprop_mode(),\
               chainer.using_config('train',False):
            temp =  model.predictor(_batch[0]).data.argmax(axis=1)
        #import ipdb;ipdb.set_trace()
        if pred is None:
            pred = temp.tolist()
            ans = _batch[1].tolist()
        else:
            pred = pred + temp.tolist()
            ans = ans + _batch[1].tolist()
    return pred,ans

class Iterator:
    def __init__(self,mode):
        self.mode = mode
        self.test = None
        self.train = None
        self.makedataset = {
            "dc":self.makedataset_dc,
            "dopc":self.makedataset_dopc,
            "dopc2":self.makedataset_dopc2,
            "po":self.makedataset_po
        }[self.mode]
        self.target = ""

    def makedataset_dc(self,articles):
        self.test = articles
        self.train = articles
        yield self.train,self.test
    
    def makedataset_dopc(self,articles):
        for i,_article in enumerate(articles):
            self.target = _article
            self.test = [_article]
            self.train = [_x for _i,_x in enumerate(articles) if _i != i]
            #import ipdb;ipdb.set_trace()
            yield self.train,self.test

    def makedataset_dopc2(self,articles):
        import random
        random.shuffle(articles)
        #print("dopc2","11記事ずつ")
        for i in range(0,len(articles),19):
            
            #import ipdb;ipdb.set_trace()
            self.target = i
            self.test = articles[i:i+19]
            self.train = [_x for _x in articles if  _x not in self.test]
            #import ipdb;ipdb.set_trace()
            yield self.train,self.test

    def makedataset_po(self,articles):
        userdict = self.makeUserDict(articles)
        for _user in userdict.keys():
            self.target = _user
            self.test = userdict[_user]
            self.train = [ _article for _article in articles if _user not in _article]
            #import ipdb;ipdb.set_trace()
            yield self.train,self.test

    def makeUserDict(self,articles):
        users = dict()
        for _article in articles:
            _user = _article.split("/")[-4]
            if _user in users.keys():
                users[_user].append(_article)
            else:
                users[_user] = [_article]
        return users


def loop(args,target,train,test):
    print("target:",target,"train:",len(train),"test:",len(test))
    # データの準備
    Features.makefeatures3(train)
    Features.makefeatures3(test)
    
    # 学習と推定
    #import ipdb;ipdb.set_trace()
    pred,ans = net(args,train,test)
    #pred,ans =  net(args,[(_data, _label) for _data,_label in zip(x_train,y_train)],[(_data, _label) for _data,_label in zip(x_test,y_test)])
    
    # log 
    with open(args.out + "/" +  args.mode + "_" + args.label + "_neural.csv","a") as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerows([[target,path,label,result]for path,label,result in zip(test,ans,pred)])

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    parser.add_argument('--label','-l',type=str, default="Interest",
                        help='比較するラベル')
    parser.add_argument('--mode','-m',type=str, default="dc",
                        help='評価方法の選択')
    parser.add_argument('--result','-r',type=bool, default=False,
                        help='結果の表示')
    parser.add_argument('--point',type=float, default=0.0,
                        help='視線の密度の閾値')
    parser.add_argument('--density','-d',type=str,          
                        default="analysis/density/density.csv",
                        help='密度のcsv')
    #parser = argparse.ArgumentParser(description='Chainer setting')
    parser.add_argument('--batchsize', '-b', type=int, default=5,
                        help='Number of images in each mini-batch')
    parser.add_argument('--epoch', '-e', type=int, default=20,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--frequency', '-f', type=int, default=-1,
                        help='Frequency of taking a snapshot')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')
    parser.add_argument('--resume', '-rr', default='',
                        help='Resume the training from snapshot')
    parser.add_argument('--unit', '-u', type=int, default=128,
                        help='Number of units')
    parser.add_argument('--noplot', dest='plot', action='store_false',
                        help='Disable PlotReport extension')
    args = parser.parse_args()

    print('GPU: {}'.format(args.gpu))
    print('# unit: {}'.format(args.unit))
    print('# Minibatch-size: {}'.format(args.batchsize))
    print('# epoch: {}'.format(args.epoch))
    print('')

    articlepaths =  glob.glob(args.path + "*/*/*/*_dom.csv") #収集した記事パス
    print("総データ：",len(articlepaths))
    articlepaths = vAnkert.checktimer("analysis/ankert/ankerts.csv",articlepaths,False)
    print("対象データ：",len(articlepaths))
    articlepaths = density.checkdensity(args.density,articlepaths,args.point)
    print("見ていないデータを省く",len(articlepaths))
    _iterator = Iterator(args.mode)
    #import ipdb;ipdb.set_trace()
    #estimate.makedataset(articlepaths)
    print("mode:",args.mode)
    #import ipdb;ipdb.set_trace()
    #train,test = estimate.makedataset(articlepaths)
    wrapper([[args,_iterator.target,train,test[:100]] for train,test in _iterator.makedataset(articlepaths)][0])
    """
    p = Pool(processes=1)
    p.map(wrapper,[[args,_iterator.target,train,test] for train,test in _iterator.makedataset(articlepaths)])
    p.close()
    """
if __name__ == "__main__":
    main()
    pass
