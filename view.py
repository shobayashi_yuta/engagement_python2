#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# ユーザの行動を記事ごとに可視化
###########################

import argparse 

import pandas as pd
import numpy as np

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as cm
import features

def drawdata_dom(data):
    domdict = {
        "main":"white",
        "hd":"red",
        "photo":"blue",
        "ynDetailRelArticle":"y",
        "ynDetailText":"c",
        "paragraph":"green",
        'ynDetailHeading':"red",
        'movie':"blue"
    }
    ax = plt.axes()
    ax.set_xlim([data[data["element"] == "main"]["left"].values[0]-40,data[data["element"] == "main"]["right"].values[0]+40])
    for i,dom in data.iterrows():
        x = np.arange(dom["left"],dom["right"])
        yTop = [dom["top"]] * len(x)
        yBottom = [dom["bottom"]] * len(x)
        ax.fill_between(x,yTop,yBottom,color=domdict[dom["element"]],alpha=0.1)
    #plt.show()

def drawdata(data,VisitTimes,isGaze=True):
    #import ipdb;ipdb.set_trace()
    #data["dtime"] = data["dtime"] - int(VisitTimes[VisitTimes["type"] == "start"]["dtime"])
    #import rolling
    #import ipdb;ipdb.set_trace()
    #interpolate = rolling.Interpolate(0.5)
    #data = interpolate.interpolate(data.drop("time",axis=1))
    isVisit = int(VisitTimes[VisitTimes["type"] == "end"]["dtime"]) - int(VisitTimes[VisitTimes["type"] == "start"]["dtime"])
    ax = plt.axes()
    init_x = data["x"].values[0]
    init_y = data["y"].values[0]
    if isGaze:
        _color = plt.get_cmap("Blues")
    else:
        _color = plt.get_cmap("Oranges")
    for x,y,t in zip(data["x"].values[1:],data["y"].values[1:],data["dtime"].values[1:]):
        ax.quiver(init_x,init_y,x-init_x,y-init_y,angles='xy',scale_units='xy',scale=1,width=0.0025,color=_color(t/isVisit))
        init_x = x
        init_y = y

    #plt.show()
        

def ChangeTomsec(timers):
    return [ChangeTime(_t) for _t in timers]

def ChangeTime(data):
    temp = data.split(":")
    #import ipdb;ipdb.set_trace()
    return int(temp[0])*60*60*1000 + int(temp[1])*60*1000 + int(temp[2])*1000 + int(temp[3])

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--id', '-i', type=int,
                        help='id')
    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    parser.add_argument('--output','-o',type=str, default="/",
                        help='グラフ出力先')
    args = parser.parse_args()

    _dir = args.path
    articlepath = args.path + str(args.id) + "_dom.csv"
    _id = str(args.id)
    #import ipdb;ipdb.set_trace()
    df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert =  features.readdataforCache(articlepath,_dir,_id)    
    df_mouse["dtime"] = df_mouse["dtime"] - df_visit["dtime"].values[0]
    df_head["dtime"] = df_head["dtime"] - df_visit["dtime"].values[0]
    """
    # 閲覧履歴のデータの取得
    df_visit = pd.read_csv(args.path + "visit.csv",names=["type","id","time","url","mode"],dtype={"type":str,"id":int,"time":str,"url":str,"mode":bool})
    df_visit = df_visit[df_visit["id"] == args.id]
    df_visit["dtime"] = ChangeTomsec(df_visit["time"])
    # 視線関係のデータの扱い
    import ipdb;ipdb.set_trace()
    import features

    df_gaze = pd.read_csv(args.path + str(args.id) + "_gaze.csv",names=["x","y","time"],dtype={"x":np.int32,"y":np.int32,"time":str})
    #df_gaze["dtime"] = ChangeTomsec(df_gaze["time"])
    #import ipdb;ipdb.set_trace()

    # マウス関係のデータの扱い
    df_mouse = pd.read_csv(args.path + str(args.id) + "_mouse.csv",names=["x","y","time"],dtype={"x":np.int32,"y":np.int32,"time":str})
    df_mouse["dtime"] = ChangeTomsec(df_mouse["time"])

    # dom関係のデータの扱い
    df_dom = pd.read_csv(args.path + str(args.id) + "_dom.csv",names=["element","content","top","bottom","left","right"],dtype={"element":str,"content":str,
                        "top":np.int32,"bottom":np.int32,"left":np.int32,"right":np.int32})
    """
    # 可視化
    plt.figure()
    drawdata_dom(df_dom)
    drawdata(df_gaze,df_visit)
    drawdata(df_mouse,df_visit,isGaze=False)
    plt.gca().invert_yaxis()
    #import ipdb;ipdb.set_trace()
    #plt.show()
    plt.savefig(args.output + str(args.id) + ".png")

    




if __name__ == "__main__":
    main()
    pass

