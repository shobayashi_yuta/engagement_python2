

import pandas as pd

def read_tfidf(path):
    '''
    wikiから作ったtfidfファイルを読み込む
    '''
    df = pd.read_csv(path,dtype={"word":str,"tf-idf":float},encoding="utf-8")
    return dict(df.values)

def NormalizationTFIDF(tfidf):
    '''
    tfidfの正規化
    '''
    maxValue = max(tfidf.values())
    #minValue = min(tfidf.values())
    minValue = 0
    for k in tfidf.keys():
        tfidf[k] = (tfidf[k] - minValue) / (maxValue - minValue)
    return tfidf

def read_wordPosition(_path):
    """
    argument: file path, {number}.csv
    return dataframe(word,top,bottom,left,right)
    """
    #print(_path)
    return pd.read_csv(_path,dtype={"word":str,"top":int,"bottom":int,"left":int,"right":int,"type":str},encoding="utf-8")

class Word:
    def __init__(self,path):
        self.path = path
        self.dic = {}

    def read_wordPosition(self,path):
        """
        argument: file path, {number}.csv
        return dataframe(word,top,bottom,left,right)
        """
        #print(_path)
        temp = pd.read_csv(path,dtype={"word":str,"top":int,"bottom":int,"left":int,"right":int,"type":str},encoding="utf-8")
        _index =  temp[(temp["word"] == "【") | (temp["word"] == "関連") | (temp["word"] == "記事") | (temp["word"] == "】") ].index
        ttt = []
        for _i in _index:
            if len(ttt) == 0:
                ttt = [_i]
            elif t == _i - 1:
                ttt.append(_i)
            else:
                ttt = []
            t = _i
        #import ipdb;ipdb.set_trace()
        if len(ttt) == 4:
            temp = temp[:ttt[0]]
        
        return temp

    def pickPageDataset(self,webpages):
        '''
        Web記事ごとに言語情報をまとめる
        args: 収録したWeb記事のpage番号とURL
        return: 辞書型({URL:[Web記事の通し番号，視線情報，記事中の言語とその位置]})
        '''
        # 記事閲覧時の視線行動のインデックス範囲
        for _i,_webpage in webpages.iterrows():
            words = self.read_wordPosition(self.path + "/%d.csv" % _webpage["page"]) 
            words = self.wordsplit(words)
            self.dic[_webpage["url"]] = words

    def wordsplit(self,_words):
        templeft = _words.loc[0,"left"]
        tempright = 850
        for i,_w in _words.iterrows():
            
            if _w["right"] > 850: # 右端が飛び出ているとき
                _words.loc[i,"right"] = tempright
                tempbom = _words.loc[i,"bottom"]
                _words.loc[i,"bottom"] = _words.loc[i-1,"bottom"]
                
                if (abs(_w["top"]-_words.loc[i-1,"bottom"])>5) & (abs(_w["left"]-_words.loc[i-1,"right"])>5 ): # 文字位置が離れている
                    templeft = _w["left"]
                    #tempright = _words.loc[i-1,"right"]

                # 単語の追加，飛び出している分の
                _words = pd.concat([_words,pd.DataFrame([[_w["word"],_words.loc[i,"bottom"],tempbom,templeft,_words.loc[i+1,"left"]]],columns = ["word", "top", "bottom","left","right"]).astype({"word":str,"top":int,"bottom":int,"left":int,"right":int})],ignore_index=True,axis=0)
                
            elif i > 0:
                #print(_w["top"],_words.loc[i-1,"bottom"])
                if (abs(_w["top"]-_words.loc[i-1,"bottom"])>5) & (abs(_w["left"]-_words.loc[i-1,"right"])>5 ):
                    #print(_w["word"],_words.loc[i-1,"word"])
                    templeft = _w["left"]
                    #tempright = _words.loc[i-1,"right"]
        
        return _words