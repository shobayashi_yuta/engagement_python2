#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 移動平均でデータを整形
###########################

import sys
import glob
import features as F
import view
import pandas as pd
import numpy as np

class Interpolate:
    def __init__(self,interval):
        self.interval = interval
    

    def interpolate(self,data):
        """
        補間
        """
        # データの区間の設定
        _time = np.arange(data["dtime"].min(),data["dtime"].max() +0.1,self.interval,dtype='float32')
        #import ipdb;ipdb.set_trace()
        
        # 一定周期に座標を変換
        temp = [data[data["dtime"] == _t].mean().tolist() for _t in _time]
        _input = pd.DataFrame(temp,columns=["x","y","dtime"]).astype(np.float32)
        _input["dtime"] = _time

        #import ipdb;ipdb.set_trace()
        timestop =  data[data["dtime"].diff(-1)*(-1) > 100]["dtime"].values # 視線が飛んでいるところの一つ手前の時間
        timestart =  data[data["dtime"].diff() > 100]["dtime"].values # 閲覧を再開した時間
        _init = 0
        #print(timestart)
        #print(timestop)
        for i,_t in enumerate(timestop):
            #print(_init,_t) 
            #import ipdb;ipdb.set_trace()
            frames = _input[(_input["dtime"] >= _init) & (_input["dtime"] <= _t)].interpolate() # 補間
            _input[frames.index[0]:frames.index[-1]+1] = frames
            _init = timestart[i]
        
        frames = _input[_input["dtime"] >= _init].interpolate() # 補間
        _input[frames.index[0]:frames.index[-1]+1] = frames
        
        #import ipdb;ipdb.set_trace()
        return _input.dropna()
        
def read_actions(dir,id):
    """
    return 閲覧履歴，dom，視線，頭部，マウス，何ページ目か
    """
    # 閲覧履歴のデータの取得
    df_visit = pd.read_csv(dir + "/visit.csv",names=["type","id","time","url","mode"],dtype={"type":str,"id":int,"time":str,"url":str,"mode":bool})
    #import ipdb;ipdb.set_trace()
    temp_df = df_visit
    df_visit = df_visit[df_visit["id"] == int(id)]
    df_visit["dtime"] =  view.ChangeTomsec(df_visit["time"])
    n_page = F.checkPages(df_visit,temp_df)

    # 視線関係のデータの扱い
    df_gaze = pd.read_csv(dir + "/" + id + "_gaze.csv",names=["x","y","time"],dtype={"x":np.int32,"y":np.int32,"time":str})
    df_gaze["dtime"] = view.ChangeTomsec(df_gaze["time"])
    df_gaze = df_gaze[df_gaze["dtime"] >= df_visit["dtime"].values[0]].drop(["time"], axis=1)
    df_gaze["dtime"] = df_gaze["dtime"] - df_visit["dtime"].values[0]
    """
    # 頭部関係のデータの扱い
    df_head = pd.read_csv(dir + "/" + id + "_head.csv",names=["x","y","z","rx","ry","rz","time"],dtype={"x":np.float32,"y":np.float32,"z":np.float32,"rx":np.float32,"ry":np.float32,"rz":np.float32, "time":str})
    df_head["dtime"] = view.ChangeTomsec(df_head["time"])
    df_head = df_head[df_head["dtime"] >= df_visit["dtime"].values[0]].drop(["time"], axis=1)
    #df_head["dtime"] = (df_head["dtime"] - df_visit["dtime"].values[0]) /(df_visit["dtime"].values[-1] - df_visit["dtime"].values[0])

    # マウス関係のデータの扱い
    df_mouse = pd.read_csv(dir + "/" + id + "_mouse.csv",names=["x","y","time"],dtype={"x":np.int32,"y":np.int32,"time":str})
    df_mouse["dtime"] = view.ChangeTomsec(df_mouse["time"])
    df_mouse = df_mouse[df_mouse["dtime"] >= df_visit["dtime"].values[0]].drop(["time"], axis=1)
    #df_mouse["dtime"] = (df_mouse["dtime"] - df_visit["dtime"].values[0]) / (df_visit["dtime"].values[-1] - df_visit["dtime"].values[0])

    # dom関係のデータの扱い
    df_dom = pd.read_csv(dir + "/" + id + "_dom.csv",names=["element","content","top","bottom","left","right"],dtype={"element":str,"content":str,
                        "top":np.int32,"bottom":np.int32,"left":np.int32,"right":np.int32},encoding="utf-8")
    """
    return df_visit,df_gaze


def main():
    directorys = sys.argv[1]
    # 収集した記事のパス
    articlepaths =  glob.glob(directorys + "*/*/*/*_dom.csv") 
    
    for article in articlepaths:
        _dir = "/".join(article.split("/")[:-1])
        _id = article.split("/")[-1].replace("_dom.csv","")
        df_visit,df_gaze = read_actions(_dir,_id)
        interpolate = Interpolate(0.5)
        interpolate.interpolate(df_gaze)
        #import ipdb;ipdb.set_trace()

if __name__ == "__main__":
    main()
    pass