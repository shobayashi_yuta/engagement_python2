#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# ユーザの行動を記事ごとに可視化
###########################

import argparse 

import pandas as pd
import numpy as np

import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as cm

from bs4 import BeautifulSoup
import lxml

import glob
import sys

def read_html(path):
    with open(path,"r",encoding="utf-8") as f:
        html = f.read()
    return html

def checkHTML_Lenge(html):
    """
    記事から文字列を算出
    """
    soup = BeautifulSoup(html,'lxml')
    #import ipdb;ipdb.set_trace()
    _text = soup.find("div",class_="articleMain")
    if _text is None:
        #import ipdb;ipdb.set_trace()
        return 0
    _text = _text.text
    return len(_text)

def numHist(Lenges):
    plt.figure()
    print(plt.hist(Lenges,range=(0,9000),bins=36,ec='black'))
    plt.savefig("temp.png")


def main():
    Lenges = []
    for i,path in enumerate(glob.glob(sys.argv[1] + "*.htm")):
        html = read_html(path) # htmlファイルの読み込み
        textLenge = checkHTML_Lenge(html) # 記事の文字数の算出
        if i % 100 == 0:
            print(path,textLenge)
        Lenges.append(textLenge) # 文字数を保存
    #import ipdb;ipdb.set_trace()
    Lenges = np.array(Lenges)
    numHist(Lenges)
    



if __name__ == "__main__":
    main()
    pass