#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# ラベルつける用の資料作成
###########################
import argparse
import sys
import os
import pandas as pd
import numpy as np
import itertools

import gazeFeatures as gF
import wordFeatures as wF
import webPage
import answer

import allocation as allo
import webPage
import matplotlib.pyplot as plt
import pickle


class Label(allo.Label):
    def iterData(self,_maindir,_targetdir): 
        
        for _i,_data in self.csv.iterrows(): # 記事ごと
            #import ipdb;ipdb.set_trace()
            #print(_data.values)
            targetarticle = str(_data.page)
            targetPage = _data.yyy
            targetdir = _targetdir + "/" + _data.user
            targetlabel = _data[self.column]
            #import ipdb;ipdb.set_trace()
            yield targetarticle,targetPage,targetdir,targetlabel
        #return 1

class Gaze(gF.Gaze):

    def pickPageDataset(self,webpage):
        '''
        Web記事ごとに視線情報をまとめる
        args: 収録したWeb記事のpage番号とURL
        return: 辞書型({URL:[Web記事の通し番号，視線情報，記事中の言語とその位置]})
        '''
        # 記事閲覧時の視線行動のインデックス範囲
        
        PageRange = [self.url.index[np.where(self.url["y"] == webpage)[0][0]]+1,self.url.index[np.where(self.url["y"] == webpage)[0][0]+1]]
        gaze =self.gazes[PageRange[0]:PageRange[1]].astype({'x':int,'y':int,'time': np.double})
        #import ipdb;ipdb.set_trace()
        #words = self.wordsplit(words)
        #print(words.columns)
        return gaze

class Word(wF.Word):
    def pickPageDataset(self,webpages):
        '''
        Web記事ごとに言語情報をまとめる
        args: 収録したWeb記事のpage番号とURL
        return: 辞書型({URL:[Web記事の通し番号，視線情報，記事中の言語とその位置]})
        '''
        # 記事閲覧時の視線行動のインデックス範囲
        for _i,_webpage in webpages.iterrows():
            words = self.read_wordPosition(self.path + "/%d.csv" % _webpage["page"]) 
            words = self.wordsplit(words)
            self.dic[_webpage["url"]] = words

class Analysis:
    """
    統計量の算出と、散布図
    """
    def calc_statistic(self,_data):
        """
        統計量の算出
        count
        mean
        std
        min
        25%
        50%
        75%
        max
        var
        """
        #import ipdb;ipdb.set_trace()
        #return pd.concat([_data.describe(),pd.DataFrame(_data.var(),columns=["var"]).T])
        return _data.describe()
        #import ipdb;ipdb.set_trace()

    def calc_words(self,_words):
        """
        単語数と，文字数を返す
        """
        wordnum = len(_words)
        wordsum = sum([len(_w) for _w in _words["word"].values if type(_w) != float])
        return wordnum,wordsum

    def pickCombi(self):
        
        featureNum = ["x","y","dx","dy","dsy","x_on","y_on","dx_on","dy_on","dsy_on"]
        statisticNum = ["mean","std","min","max","75%","var"]
        return list(itertools.product(featureNum,statisticNum))

    def setFeature_Statistics(self,_columns,_index):
        self.statistics = _index
        self.features = _columns
        
    def calcCoverageofsentences(self,_gaze,_word):
        """
        視線が文章上にあるかどうか,割合,文章を見ている時間，見ている時間の割合
        """
        
        _top = _word.min()["top"]
        _left = _word.min()["left"]
        _bottom = _word.max()["bottom"]
        _right = _word.max()["right"]
        on_sentences = _gaze[(_gaze["x"] >= _left) & (_gaze["x"] <= _right) & (_gaze["y"] >= _top) & (_gaze["y"] <= _bottom) ]
        if len(on_sentences) != 0:
            onTime = self.calcTime(on_sentences.index,on_sentences["time"])
        else:
            onTime = 0
        if len(_gaze) != 0:
            Time = self.calcTime(_gaze.index,_gaze["time"])
        else:
            Time = 0
        return on_sentences,onTime,Time

    def calcTime(self,_index,_timelist):
        tmp_i = _index[0]
        tmp_time = _timelist[tmp_i]
        tmp_sum = 0
        for _i in _index[1:]:
            if _i - tmp_i == 1:# 連番
                tmp_sum += (_timelist[_i] - tmp_time)
            tmp_time = _timelist[_i]
            tmp_i = _i
        
        return tmp_sum




    def pick_minigazedata(self,_gazes,xmin,xmax,ymin,ymax):
        #init_x = _gazes["x"].values[0]
        #init_y = _gazes["y"].values[0]
        _gazes["sy"] = _gazes["y"].shift() # 前の値を持ってきているだけ
        _gazes["sx"] = _gazes["x"].shift() # 前の値を持ってきているだけ
        _gazes["dy"] = _gazes["y"].diff() # 差分
        _gazes["dx"] = _gazes["x"].diff() # 差分
        _gazes["dt"] = _gazes["time"] - _gazes["time"].values[0]
        _gazes["dsy"] = np.sqrt(np.power(_gazes["dy"],2) + np.power(_gazes["dx"],2))
        #import ipdb;ipdb.set_trace()
        _gazes = _gazes[1:]
        tmp_gazes = _gazes[(gF.crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmin,ymin]]*len(_gazes)),np.array([[xmin,ymax]]*len(_gazes)),np) | gF.crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmax,ymin]]*len(_gazes)),np.array([[xmax,ymax]]*len(_gazes)),np) | gF.crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmin,ymax]]*len(_gazes)),np.array([[xmax,ymax]]*len(_gazes)),np) | gF.crossjudge_cross(_gazes[["sx","sy"]].values,_gazes[["x","y"]].values,np.array([[xmax,ymin]]*len(_gazes)),np.array([[xmin,ymin]]*len(_gazes)),np)) | (((_gazes["sx"] >= xmin) & (_gazes["sx"] <= xmax) & (_gazes["sy"] >= ymin) & (_gazes["sy"] <= ymax)) | ((_gazes["x"] >= xmin) & (_gazes["x"] <= xmax) & (_gazes["y"] >= ymin) & (_gazes["y"] <= ymax)))]
        
        ## 20181009現在，ミニ記事に入る視線行動のみを対象，途中で一旦抜けているものは省いている
        #print(tmp_gazes["time"].values)
        if len(tmp_gazes["time"]) != 0:
            tmp_gazes["dt"] = tmp_gazes["time"] - tmp_gazes["time"].values[0] # ミニ記事ごとに視線の時系列を示す
            tmp_gazes["x"] = tmp_gazes["x"] - xmin # ミニ記事の左上が原点になるように調節
            tmp_gazes["y"] = tmp_gazes["y"] - ymin # ミニ記事の左上が原点になるように調節
        return tmp_gazes            

    def pick_miniworddata(self,_words,xmin,xmax,ymin,ymax):
        tmp_word = _words[((_words["left"] +_words["right"])/2 >= xmin) & ((_words["left"] +_words["right"])/2 <= xmax) & ((_words["top"]+_words["bottom"])/2 >= ymin) & ((_words["top"]+_words["bottom"])/2 <= ymax)]
        tmp_word["right"] = tmp_word["right"] - xmin # ミニ記事の左上が原点になるように調節
        tmp_word["left"] = tmp_word["left"] - xmin # ミニ記事の左上が原点になるように調節
        tmp_word["top"] = tmp_word["top"] - ymin # ミニ記事の左上が原点になるように調節
        tmp_word["bottom"] = tmp_word["bottom"] - ymin # ミニ記事の左上が原点になるように調節
        return tmp_word

def main():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--data', '-d', type=str, default='data',
                        help='データディレクトリ')
    parser.add_argument('--column', '-c', default='understanding',
                        help='対象ラベル名')
    parser.add_argument('--outputs', '-o', default='temp',
                        help='保存ディレクトリ')
    parser.add_argument('--label', '-l', default='label_split.csv',
                        help='ラベルのcsvファイル(split)')
    args = parser.parse_args()
    """
    if os.path.exists('statistic.pickle'):
        with open('statistic.pickle', mode='rb') as f:
            datadic = pickle.load(f)
    
    else:
    """
    analysis = Analysis()
    label = Label(args.label,args.column)
    label.makeDirectorys(args.outputs)
    datadic = dict()
    #datas = list()
    c = 0
    featurelist = None
    statisticslist = None
    for _article,_page,_dir,_label in  label.iterData(args.outputs,args.data): # mini記事ごとの処理
        gaze = Gaze(_dir)
        word = Word(_dir)
           
        #print(gaze.url["y"])
        webpages = webPage.getWebPage(_dir)
        #import ipdb;ipdb.set_trace()
        url = webpages[webpages["page"] == int(_article)]["url"].values[0]
        name = _dir.split("/")[-2]
        print(_article,_page,_dir,url)
        # 視線データと文章データの取得analysis.pickComb
            
        gazedata = gaze.pickPageDataset(url)
        gazedata = analysis.pick_minigazedata(gazedata,150,900,_page,_page+300)
        worddata = word.read_wordPosition(word.path + "/%d.csv" % int(_article)) 
        worddata = word.wordsplit(worddata)
        worddata = analysis.pick_miniworddata(worddata,150,900,_page,_page+300)
        on_sentences,ontime,allTime = analysis.calcCoverageofsentences(gazedata,worddata) # 文章上に視線がある時間，ページ上に視線がある時間
        if allTime != 0:
            time_percent = ontime / allTime
        else:
            time_percent = 0 
        # 特徴量の設定
        statistic_gaze = analysis.calc_statistic(gazedata)
        statistic_word = analysis.calc_statistic(worddata)
        statistic_onsentence = analysis.calc_statistic(on_sentences)
        wordnum,wordsum = analysis.calc_words(worddata)
        if featurelist is None:
            featurelist = statistic_gaze.columns
        if statisticslist is None:
            statisticslist = statistic_gaze.index
        #import ipdb;ipdb.set_trace()
        features = {"dt":statistic_gaze["dt"],"x":statistic_gaze["x"],"y":statistic_gaze["y"],"dx":statistic_gaze["dx"],"dy":statistic_gaze["dy"],"dsy":statistic_gaze["dsy"],
                    "x_on":statistic_onsentence["x"],"y_on":statistic_onsentence["y"],"dx_on":statistic_onsentence["dx"],"dy_on":statistic_onsentence["dy"],"dsy_on":statistic_onsentence["dsy"],        
                    "word":statistic_word,"wordnum":wordnum,"moji":wordsum,"ontime":ontime,"alltime":allTime,"time_percent":time_percent,"label":_label}
        if _label not in datadic.keys():
            datadic[_label] = [features]
            #datadic[_label] = [[statistic_gaze,statistic_word,wordnum,wordsum,ontime,allTime,time_percent,_label]]
            #datadic[name] = [[statistic_gaze.at["mean","x"],statistic_gaze.at["mean","y"]]]
        else:
            datadic[_label].append(features)
            #datadic[_label].append([statistic_gaze,statistic_word,wordnum,wordsum,ontime,allTime,time_percent,_label])
            #datadic[name].append([statistic_gaze.at["mean","x"],statistic_gaze.at["mean","y"]])
        
        #datas.append([statistic_gaze,statistic_word.values,wordnum,wordsum,_label])
        """
        with open('statistic.pickle', mode='wb') as f:
            pickle.dump(datadic, f)
        """
        """
        if c > 5:
            break
        c+=1
        """
        
    #analysis.setFeature_Statistics(sorted(datadic.values())[0][0][0].columns,sorted(datadic.values())[0][0][0].index)
    features = analysis.pickCombi()
    #import ipdb;ipdb.set_trace()
    features.append(("dt","max"))
    features.append(("moji"))
    features.append(("wordnum"))
    features.append(("ontime"))
    features.append(("alltime"))
    features.append(("time_percent"))

    for _combi in list(itertools.combinations(features,2)):
        if _combi[0][0] == _combi[1][0]:
            continue
        print(_combi)
        #import ipdb;ipdb.set_trace()
        # 描画
        xlabel = "_".join(_combi[0]) if len(_combi[0]) == 2 else _combi[0]
        ylabel = "_".join(_combi[1]) if len(_combi[1]) == 2 else _combi[1]

        plt.figure()
        for i,label in enumerate(["非理解","大雑把","理解"]):
            #import ipdb;ipdb.set_trace()
            x = [data[_combi[0][0]][_combi[0][1]] if len(_combi[0]) == 2 else data[_combi[0]] for data in datadic[i+1] ]
            y = [data[_combi[1][0]][_combi[1][1]] if len(_combi[1]) == 2 else data[_combi[1]] for data in datadic[i+1] ]
            
            
            plt.scatter(x,y,s=5,label=label)

            """
            if "moji" in _combi and "wordnum" in _combi:
                    x = [data[3] for data in datadic[i+1]]
                    y = [data[2] for data in datadic[i+1]]

            elif "moji" not in _combi and "wordnum" not in _combi:
                x = [data[0].at[_combi[0][1],_combi[0][0]] for data in datadic[i+1]]
                y = [data[0].at[_combi[1][1],_combi[1][0]] for data in datadic[i+1]]
            else:
                x = [data[0].at[_combi[0][1],_combi[0][0]] for data in datadic[i+1]]
                if "moji" in _combi:
                    y = [data[3] for data in datadic[i+1]]
                else:
                    y = [data[2] for data in datadic[i+1]]
            #label = [str(data[4]) for data in datadic[i+1]]
            plt.scatter(x,y,s=5,label=label)
            plt.xlabel("_".join(_combi[0]))
            plt.ylabel("_".join(_combi[1]))
            """
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        #plt.ylabel("_".join(_combi[1]))

        plt.show()
        plt.legend()
        plt.savefig(args.outputs + "/" + xlabel + "-" + ylabel + ".png")
        #import sys;sys.exit()
        plt.close()
    """
    # 描画
    plt.figure()
    
    for _name in datadic.keys():
        temp = np.array(datadic[_name]).T
        plt.scatter(temp[0],temp[1],label=_name)
    plt.xlabel("mean")
    plt.xlabel("mean")
    plt.legend()
    plt.savefig("temp.png")
    #plt.show()
    #import ipdb;ipdb.set_trace()
    """
    



    """
    path = sys.argv[1]
    webpages = webPage.getWebPage(path) 
    ans  = answer.Answer(path)
    ans.addPageNumber(webpages)
    word = wF.Word(path)
    word.pickPageDataset(webpages)
    ans.answers["user"] = "/".join(path.split("/")[-2:])
    data = ans.answers[["user","news","page","word","answer"]]
    data = splitPage(data)

    minidata = splitminiPage(word,webpages,data)
    """

if __name__ == "__main__":
    main()
    pass