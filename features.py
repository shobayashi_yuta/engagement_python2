#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# ユーザ行動の分布
# 特徴量と統計量
###########################


import argparse
import glob
import pandas as pd
import view
import numpy as np
import re
import itertools
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm  as cm
import MeCab
import density
import os
import pickle
import view_perAnkert as vAnkert
import rolling

#cache = "/mnt/data/shobayashi/"
cache = ""


class Analysis:
    def __init__(self):
        self.dataset = dict()
        self.label = []

    def calc_statistic(self,_data):
        """
        統計量の算出
        count
        mean
        std
        min
        25%
        50%
        75%
        max
        """
        #import ipdb;ipdb.set_trace()
        
        #temp = pd.concat([_data.describe()[1:],pd.DataFrame(_data.var(),columns=["var"]).T]).replace(np.inf,0).fillna(0).replace(-np.inf,0)
        temp = _data.describe().replace(np.inf,0).fillna(0).replace(-np.inf,0)
        #dt = temp["dt"]["max"]
        #temp = temp.drop("dt",axis=1)
        #temp.loc["max","dt"] = dt
        return temp 
        #return _data.describe()[1:] #countを省く

    def calc_statisticForDom(self,_data,_doms):
        df = {
            "x:mean":[],
            "x:std":[],
            "x:min":[],
            "x:max":[],
            "y:mean":[],
            "y:std":[],
            "y:min":[],
            "y:max":[],
            "dxp:mean":[],
            "dxp:std":[],
            "dxp:min":[],
            "dxp:max":[],
            "dyp:mean":[],
            "dyp:std":[],
            "dyp:min":[],
            "dyp:max":[],
            "dtp:sum":[]
        }
        for _,paragraph in _doms[_doms["element"] == "paragraph"].iterrows():
            temp = _data.copy()
            temp["y"] = (_data["y"] - paragraph.top) / (paragraph.bottom - paragraph.top)
            
            temp["dtp"] = temp["dt"].diff(-1) * (-1)
            temp["dyp"] = temp["y"].diff() / temp["dt"].diff()# 差分
            temp["dxp"] = temp["x"].diff() / temp["dt"].diff() # 差分
            #temp["dyp"] = temp["y"].diff()# 差分
            #temp["dxp"] = temp["x"].diff() # 差分

            temp = temp.replace(np.inf,0).fillna(0)
            #import ipdb;ipdb.set_trace()
            temp["dxyp"] = np.sqrt(np.power(temp["dyp"],2) + np.power(temp["dxp"],2))
            temp = temp[ #(_data["x"] <= paragraph["right"]) 
                                #& (_data["x"] >= paragraph["left"]) 
                                (temp["y"] >= 0) 
                                & (temp["y"] <= 1) ]
            temp_desc = temp.describe().drop(["25%","50%","75%"]).drop("count").fillna(0).replace(np.inf,0).replace(-np.inf,0)
            
            # パラグラフの特徴量
            df["dtp:sum"].append(temp["dtp"].sum() /(paragraph.bottom - paragraph.top))
            for key in df.keys():
                if key == "dtp:sum":
                    continue
                df[key].append(temp_desc[key.split(":")[0]][key.split(":")[1]])
        df = pd.DataFrame(df).describe().drop(["count","25%","50%","75%"])
        dff = dict()
        for i,j in itertools.product(df.columns,df.index):
            dff["_".join((i,j))] = df[i][j]
        return dff
        #import ipdb;ipdb.set_trace()
            
    def makeArray(self,data,action):
        """
        pandas を辞書に，一列に
        """
        temp = dict()
        for key in data.keys():
            for index in data[key].keys():
                if index == "count":
                    continue
                temp[key + "_" + index + ":" + action] = data[key][index]
        #import ipdb;ipdb.set_trace()
        #temp["count" +  ":" + action] = data[key]["count"]
        return temp

    def pickCombi(self,_data):
        featureNum = _data.columns
        statisticNum = _data.index
        _list = list(itertools.product(featureNum,statisticNum)) 
        #return [tuple([_action]) + f for f in _list ]
        return _list

    def addtodataset(self,action,data):
        if action not in self.dataset.keys():
            self.dataset[action] = dict() 
        for key in self.pickCombi(data):
            _feature = data[key[0]][key[1]]
            if "_".join(key)  in self.dataset[action].keys():
                self.dataset[action]["_".join(key)].append(_feature)
            else:
                self.dataset[action]["_".join(key)] = [_feature]
    
    def addtodataset2(self,action,data):
        if action not in self.dataset.keys():
            self.dataset[action] = dict()
        for key in data.keys():
            if key in self.dataset[action].keys():
                self.dataset[action][key].append(data[key])
            else:
                self.dataset[action][key] = [data[key]]

    def addtodataset3(self,action,data):
        if action not in self.dataset.keys():
            self.dataset[action] = data
        else:
            self.dataset[action] = pd.concat([self.dataset[action],data],ignore_index=False) 
            self.dataset[action].index = range(len(self.dataset[action]))
        #import ipdb;ipdb.set_trace()

    def plot(self,x,y,label):
        plt.figure()
        plt.scatter(x, y, vmin=0, vmax=max(label), c=label, cmap=cm.Blues, linewidths=0.5, edgecolors="black")
        

    def plotfeatures2(self,dataset,labels,target,_type):
        labels = labels[target].values
        for _combi in list(itertools.combinations(dataset.keys(),2)):
            #import ipdb;ipdb.set_trace()
            """
            if _combi[0].split("_")[0]  == _combi[1].split("_")[0]:
                continue
            """
            print(_combi)
            #print(_combi)
            # 描画
            x = np.array(dataset[_combi[0]])
            y = np.array(dataset[_combi[1]])
            self.plot(x, y, labels)
            #import ipdb;ipdb.set_trace()
            plt.xlim([sorted(x, reverse=False)[10],sorted(x, reverse=True)[10]])
            plt.ylim([sorted(y, reverse=False)[10] , sorted(y, reverse=True)[10]])
            #plt.show()
            plt.xlabel(_combi[0])
            plt.ylabel(_combi[1])
            #plt.legend()
            plt.tight_layout()
            #import ipdb;ipdb.set_trace()
            plt.savefig("analysis/" + _type + "/" + _combi[0] + "-" + _combi[1] + "_" + target + ".png")
            plt.close()

    def Normalization_dataFordom(self,data,dom):
        """
        記事の大きさに対して正規化
        """
        #import ipdb;ipdb.set_trace()
        right = dom["right"].max()
        left = dom["left"].min()
        top = dom["top"].min()
        bottom = dom["bottom"].max()
        data["x"] = (data["x"] - left) / (right -left)
        data["y"] = (data["y"] - top) / (bottom-top)
        return data
    
    def Normalization_PerHeight(self,data,height):
        return data / height

            
class Dom:
    def __init__(self):
        self.tagger = MeCab.Tagger('-Ochasen')
        self.tagger.parse("")

    def add_fetures(self,doms):
        features = dict()       
        hd = doms[doms["element"] == "hd"]
        detailtext = doms[doms["element"] == "ynDetailText"] # 複数の可能性あり
        features["hdheight"] =  self.domSize(hd) 
        features["textheight"] =  self.domSize(detailtext)
        features["moji"] = self.mojicount(hd)
        features["moji"] += self.mojicount(detailtext)
        features["words"] = self.wordcount(hd)
        features["words"] += self.wordcount(detailtext)
        features["height"] = self.domSize(doms)
        # 本文の範囲 (全体の左上を原点)
        features["textright"] = (detailtext["right"].max()- doms["left"].min()) / (doms["right"].max()  - doms["left"].min()) 
        features["textleft"] = (detailtext["left"].min() - doms["left"].min()) / (doms["right"].max()  - doms["left"].min())
        features["texttop"] = (detailtext["top"].min()- doms["top"].min()) / features["height"]
        features["textbottom"] = (detailtext["bottom"].max()- doms["top"].min()) / features["height"]
        #import ipdb;ipdb.set_trace()
        return features

    def domSize(self,_doms):
        return _doms["bottom"].max()  - _doms["top"].min()

    def mojicount(self,_doms):
        _sum = 0
        for i,dom in _doms.iterrows():
            _sum += len(dom["content"].replace("\t",""))
        return _sum
    
    def wordcount(self,_doms):
        """
        形態素数を返す
        """
        _sum = 0
        for i,dom in _doms.iterrows():
            if type(dom["content"]) == float:
                continue
            #import ipdb;ipdb.set_trace()
            #dom["content"].replace("\t","")
            node = self.tagger.parseToNode(dom["content"].replace("\t",""))
            tokens = []
            while node:
                category,sub_category = node.feature.split(',')[:2]
                #print(node.surface,node.feature,category,sub_category)
                # 固有名詞または一般名詞の場合のみtokensに追加する
                #if category == '名詞' and sub_category in ("固有名詞", "一般","サ変接続"):
                #print(node.surface)
                #try:
                if node.surface != "" and node.surface != " ":
                    tokens.append(node.surface)
                #except:
                #    print("unicode error")   
                node = node.next
                    #import ipdb;ipdb.set_trace()
            _sum += len(tokens)
        return _sum

    def wordcount2(self,dom):
        if type(dom["content"]) == float:
            return 0
            #import ipdb;ipdb.set_trace()
            #dom["content"].replace("\t","")
        node = self.tagger.parseToNode(dom["content"].replace("\t",""))
        tokens = []
        while node:
            category,sub_category = node.feature.split(',')[:2]
            #print(node.surface,node.feature,category,sub_category)
            # 固有名詞または一般名詞の場合のみtokensに追加する
            #if category == '名詞' and sub_category in ("固有名詞", "一般","サ変接続"):
            #print(node.surface)
            #try:
            if node.surface != "" and node.surface != " ":
                tokens.append(node.surface)
            #except:
            #    print("unicode error")   
            node = node.next
                #import ipdb;ipdb.set_trace()
        return len(tokens)

    def Normalization(self,doms):
        """
        正規化
        """
        #import ipdb;ipdb.set_trace()
        _doms = doms.copy()
        _doms["top"] =  (doms["top"] - doms["top"].min()) / (doms["bottom"].max() - doms["top"].min())
        _doms["bottom"] =  (doms["bottom"] - doms["top"].min()) / (doms["bottom"].max() - doms["top"].min())
        _doms["right"] =  (doms["right"] - doms["left"].min()) / (doms["right"].max() - doms["left"].min())
        _doms["left"] =  (doms["left"] - doms["left"].min()) / (doms["right"].max() - doms["left"].min())
        
        return _doms

    def add_features2(self,doms):
        domdict = {
        "main":0,
        "hd":1,
        "photo":2,
        "ynDetailRelArticle":3,
        "ynDetailText":4,
        "paragraph":5,
        'ynDetailHeading':6,
        'movie':7
        }
        _doms = doms.copy()
        for  i,_d in doms.iterrows():
            #import ipdb;ipdb.set_trace()
            _doms.ix[i,"word"] = self.wordcount2(_d)
            _doms.ix[i,"element_id"] = domdict[_d["element"]]
        return _doms
        


class Head:
    def add_speed(self,_heads):
        #_heads["dt"] = _heads["dtime"] - _heads["dtime"].values[0]
        #_heads = _heads[_heads["dt"] >= 0].drop(["time","dtime"], axis=1)
        #_heads["sy"] = _heads["y"].shift() # 前の値を持ってきているだけ
        #_heads["sx"] = _heads["x"].shift() # 前の値を持ってきているだけ
        #_heads["sz"] = _heads["z"].shift() # 前の値を持ってきているだけ
        _heads["dy"] = _heads["y"].diff() # 差分
        _heads["dx"] = _heads["x"].diff() # 差分
        _heads["dz"] = _heads["z"].diff() # 差分
        #_heads["sry"] = _heads["ry"].shift() # 前の値を持ってきているだけ
        #_heads["srx"] = _heads["rx"].shift() # 前の値を持ってきているだけ
        #_heads["srz"] = _heads["rz"].shift() # 前の値を持ってきているだけ
        _heads["dry"] = _heads["ry"].diff() # 差分
        _heads["drx"] = _heads["rx"].diff() # 差分
        _heads["drz"] = _heads["rz"].diff() # 差分
        #import ipdb;ipdb.set_trace()
        
        _heads["dsy"] = np.sqrt(np.power(_heads["dy"],2) + np.power(_heads["dx"],2) + np.power(_heads["dz"],2))
        return _heads
    
    def Normalization(self,data):
        data["x"] = (data["x"] - data["x"].mean())/data["x"].std()
        data["y"] = (data["y"] - data["y"].mean())/data["y"].std()
        data["z"] = (data["z"] - data["z"].mean())/data["z"].std()
        data["rx"] = (data["rx"] - data["rx"].mean())/data["rx"].std()
        data["ry"] = (data["ry"] - data["ry"].mean())/data["ry"].std()
        data["rz"] = (data["rz"] - data["rz"].mean())/data["rz"].std()
        return data



class Mouse:
    def NormalizationFordom(self,data,dom):
        """
        記事の大きさに対して正規化
        """
        #import ipdb;ipdb.set_trace()
        right = dom["right"].max()
        left = dom["left"].min()
        top = dom["top"].min()
        bottom = dom["bottom"].max()
        data["x"] = (data["x"] - left) / (right -left)
        data["y"] = (data["y"] - top) / (bottom-top)
        return data

    def add_speed(self,_mouses):
        _mouses["dt"] = _mouses["dtime"] - _mouses["dtime"].values[0]
        #_mouses = _mouses[_mouses["dt"] >= 0].drop(["time","dtime"], axis=1)
        #_mouses["sy"] = _mouses["y"].shift() # 前の値を持ってきているだけ
        #_mouses["sx"] = _mouses["x"].shift() # 前の値を持ってきているだけ
        _mouses["dy"] = _mouses["y"].diff() # 差分
        _mouses["dx"] = _mouses["x"].diff() # 差分
        #import ipdb;ipdb.set_trace()
        
        _mouses["dsy"] = np.sqrt(np.power(_mouses["dy"],2) + np.power(_mouses["dx"],2))
        return _mouses


class Gaze:
    def NormalizationFordom(self,data,dom):
        """
        記事の大きさに対して正規化
        """
        #import ipdb;ipdb.set_trace()
        right = dom["right"].max()
        left = dom["left"].min()
        top = dom["top"].min()
        bottom = dom["bottom"].max()
        data["x"] = (data["x"] - left) / (right -left)
        data["y"] = (data["y"] - top) / (bottom-top)
        return data

    def add_speed(self,_gazes):
        #_gazes["dt"] = _gazes["dtime"] - _gazes["dtime"].values[0]
        #_gazes = _gazes[_gazes["dt"] >= 0].drop(["time","dtime"], axis=1)
        #import ipdb ;ipdb.set_trace()
        #_gazes["sy"] = _gazes["y"].shift() # 前の値を持ってきているだけ
        #_gazes["sx"] = _gazes["x"].shift() # 前の値を持ってきているだけ
        #import ipdb;ipdb.set_trace()
        _gazes["dy"] = _gazes["y"].diff()# 差分
        _gazes["dx"] = _gazes["x"].diff()# 差分
        #import ipdb;ipdb.set_trace()
        _gazes = _gazes.replace(np.inf,0).fillna(0).replace(-np.inf,0)
        #import ipdb;ipdb.set_trace()
        _gazes["dsy"] = np.sqrt(np.power(_gazes["dy"],2) + np.power(_gazes["dx"],2))
        #import ipdb;ipdb.set_trace()
        return _gazes
    
    def calc_move(self,_gazes):
        temp = _gazes.copy()
        #_gazes["dt"] = temp["dtime"] - temp["dtime"].values[0]
        temp["ddy"] = temp["y"].diff()# 差分
        temp["ddx"] = temp["x"].diff()# 差分
        temp["ddsy"] = np.sqrt(np.power(temp["ddy"],2) + np.power(temp["ddx"],2))
        #temp = pickdatafordom(temp,doms)
        move = temp["ddsy"].sum()
        return move

    def add_direction(self,_gazes):
        _gazes["dx-"] = _gazes[_gazes["dx"] < 0]["dx"]
        _gazes["dx+"] = _gazes[_gazes["dx"] > 0]["dx"]
        _gazes["dy-"] = _gazes[_gazes["dy"] < 0]["dy"]
        _gazes["dy+"] = _gazes[_gazes["dy"] > 0]["dy"]
        return _gazes
    
    def add_fft2(self,_data):
        F = np.fft.fft2(_data)
        Amp = np.abs(F)
        #import ipdb;ipdb.set_trace()
        return np.array_split(Amp,2,axis=1)

    def histgram2d(self,_data):
        temp =  pd.DataFrame(np.histogram2d(_data["x"].values,_data["y"].values,range=[[0.0,1.0],[0.0,1.0]],bins=[3,10],density=True)[0].reshape(1,-1))
        temp.columns = ["hist" + str(i) + ":gaze" for i in range(len(temp.columns))] 
        return temp.T.to_dict()[0]
        #import ipdb;ipdb.set_trace()


def read_actions(dir,id):
    """
    return 閲覧履歴，dom，視線，頭部，マウス，何ページ目か
    """
    # 閲覧履歴のデータの取得
    df_visit = pd.read_csv(dir + "/visit.csv",names=["type","id","time","url","mode"],dtype={"type":str,"id":int,"time":str,"url":str,"mode":bool})
    #import ipdb;ipdb.set_trace()
    temp_df = df_visit
    df_visit = df_visit[df_visit["id"] == int(id)]
    df_visit["dtime"] =  view.ChangeTomsec(df_visit["time"])
    n_page = checkPages(df_visit,temp_df)

    # 視線関係のデータの扱い
    df_gaze = pd.read_csv(dir + "/" + id + "_gaze.csv",names=["x","y","time"],dtype={"x":np.int32,"y":np.int32,"time":str})
    df_gaze["dtime"] = view.ChangeTomsec(df_gaze["time"])
    df_gaze = df_gaze[df_gaze["dtime"] >= df_visit["dtime"].values[0]].drop(["time"], axis=1)
    df_gaze["dtime"] = df_gaze["dtime"] - df_visit["dtime"].values[0]
    # 視線データの補間
    interpolate = rolling.Interpolate(0.5)
    df_gaze = interpolate.interpolate(df_gaze)

    #df_gaze["dtime"] = (df_gaze["dtime"]) / (df_visit["dtime"].values[-1] - df_visit["dtime"].values[0])
    
    
    # 頭部関係のデータの扱い
    df_head = pd.read_csv(dir + "/" + id + "_head.csv",names=["x","y","z","rx","ry","rz","time"],dtype={"x":np.float32,"y":np.float32,"z":np.float32,"rx":np.float32,"ry":np.float32,"rz":np.float32, "time":str})
    df_head["dtime"] = view.ChangeTomsec(df_head["time"])
    df_head = df_head[df_head["dtime"] >= df_visit["dtime"].values[0]].drop(["time"], axis=1)
    df_head["dtime"] = df_head["dtime"] - df_visit["dtime"].values[0]

    # マウス関係のデータの扱い
    df_mouse = pd.read_csv(dir + "/" + id + "_mouse.csv",names=["x","y","time"],dtype={"x":np.int32,"y":np.int32,"time":str})
    df_mouse["dtime"] = view.ChangeTomsec(df_mouse["time"])
    df_mouse = df_mouse[df_mouse["dtime"] >= df_visit["dtime"].values[0]].drop(["time"], axis=1)
    df_mouse["dtime"] = df_mouse["dtime"] - df_visit["dtime"].values[0]

    # dom関係のデータの扱い
    df_dom = pd.read_csv(dir + "/" + id + "_dom.csv",names=["element","content","top","bottom","left","right"],dtype={"element":str,"content":str,
                        "top":np.int32,"bottom":np.int32,"left":np.int32,"right":np.int32},encoding="utf-8")

    return df_visit,df_dom,df_gaze,df_head,df_mouse,n_page

def checkPages(_dfvisit,_all):
    """
    何ページ目かを返す，便宜上最初のページを０ページとする
    """
    _url = _dfvisit["url"].values[0]
    pat = re.sub('&p=\d',"",_url)
    return _dfvisit["id"].values[0] - int(_all[_all["url"] == pat]["id"])


def read_ankert(path):
    ankert = pd.read_csv(path)
    hasinterest= {
        "興味深かった":3,
        "どちらかといえば興味深かった":2,
        "どちらかといえば興味が湧かなかった":1,
        "興味が湧かなかった":0
    }
    polite= {
        "丁寧に読めた":4,
        "どちらかといえば丁寧に読めた":3,
        "どちらかといえば流し読みをした":2,
        "流し読みをした":1,
        "ほとんど読まなかった":0
    }
    Concentration = {
        "集中できた":3,
        "どちらかといえば集中できた":2,
        "どちらかといえば集中できなかった":1,
        "集中できなかった":0
    }
    know = {
        "知っていた":1,
        "知らない":0
    }
    hasknow = {
        "もっと知りたい":1,
        "もう内容について知りたいことはない":0
    }
    difficult = {
        "難しかった":2,
        "どちらとも言えない":1,
        "簡単だった":0
    }
    
    ankert.at[ankert.index[0],"Interest"] = hasinterest[ankert.at[ankert.index[0],"Interest"]]
    ankert.at[ankert.index[0],"Polite"] = polite[ankert.at[ankert.index[0],"Polite"]]
    ankert.at[ankert.index[0],"KnowInput"] = know[ankert.at[ankert.index[0],"KnowInput"]]
    ankert.at[ankert.index[0],"hasKnow"] = hasknow[ankert.at[ankert.index[0],"hasKnow"]]
    ankert.at[ankert.index[0],"Concentration"] = Concentration[ankert.at[ankert.index[0],"Concentration"]]
    ankert.at[ankert.index[0],"Difficult"] = difficult[ankert.at[ankert.index[0],"Difficult"]]
    
    return ankert

def read_preankert(dir):
    _interestpath = glob.glob(dir + "/Interest*")[0] # 最初のアンケート
    _uninterestpath = glob.glob(dir + "/unInterest*")[0] #最初のアンケート
    _interest = pd.read_csv(_interestpath,names=["id","genre","pre",""],dtype={"id":np.int32,"genre":str,"pre":str})
    _uninterest = pd.read_csv(_uninterestpath,names=["id","genre","pre",""],dtype={"id":np.int32,"genre":str,"pre":str})
    know = {
        "知りたい":3,
        "どちらかといえば知りたい":2,
        "どちらかといえば知りたいと思わない":1,
        "知りたいと思わない":0
    }
    for index,_ankert in _interest.iterrows():
        _interest.at[index,"pre"] = know[_interest.at[index,"pre"][1:]] # 何故か前に空白が入っているため[1:]
    for index,_ankert in _uninterest.iterrows():
        _uninterest.at[index,"pre"] = know[_uninterest.at[index,"pre"][1:]]
    return [_interest,_uninterest]

def makefeatures2(articlepaths):
    """
    記事のパスリストにしたがって，
    特徴量をまとめる
    """
    gF = Gaze()
    mF = Mouse()
    dF = Dom()
    hF = Head()
    analysis = Analysis()
    datasets = None
    ankerts = None
    for articlepath in articlepaths:
        """
        記事ごとに処理
        """
        #print(articlepath)
        _dir = "/".join(articlepath.split("/")[:-1])
        _id = articlepath.split("/")[-1].replace("_dom.csv","")
        # キャッシュを考慮してデータを読み込む
        df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert =  readdataforCache(articlepath,_dir,_id)
        #df_mouse["dtime"] = df_mouse["dtime"] - df_visit["dtime"].values[0]
        #df_head["dtime"] = df_head["dtime"] - df_visit["dtime"].values[0]
        #import ipdb;ipdb.set_trace()

        """
        # 時間部分の正規化
        df_gaze["dtime"] = Normalization_toTime(df_gaze["dtime"],df_visit["dtime"].values)
        df_head["dtime"] = Normalization_toTime(df_head["dtime"],df_visit["dtime"].values)
        df_mouse["dtime"] = Normalization_toTime(df_mouse["dtime"],df_visit["dtime"].values)
        """

        # 記事に対する正規化
        gaze_norm = gF.NormalizationFordom(df_gaze,df_dom)
        mouse_norm = mF.NormalizationFordom(df_mouse,df_dom)
        head_norm = hF.Normalization(df_head)
        dom_norm = dF.Normalization(df_dom)
        
        """
        特徴量の追加
        """
        # 差分
        gaze_norm = gF.add_speed(gaze_norm)
        head_norm = hF.add_speed(head_norm)
        # 視線のヒストグラム
        gazehist = gF.histgram2d(df_gaze[["x","y"]])
        #import ipdb;ipdb.set_trace()
        # 閲覧時間
        visit_time =  df_visit["dtime"].diff().values[-1] # 閲覧時間
        #visit_timeh = visit_time / dF.domSize(df_dom)
        visit_timeWord = visit_time / dF.wordcount(df_dom) # 閲覧時間を単語数で割った
        # 文字
        move_word = gF.calc_move(gaze_norm) / dF.wordcount(df_dom) 
        move_time =  gF.calc_move(gaze_norm) / visit_time
        
        # 時系列データの整形
        #import ipdb;ipdb.set_trace()
        gaze_norm = gaze_norm.drop("dtime",axis=1) 
        mouse_norm =  mouse_norm.drop("dtime",axis=1) 
        head_norm = head_norm.drop("dtime",axis=1) 

        gaze_sta = analysis.calc_statistic(gaze_norm)
        gaze_sta = analysis.makeArray(gaze_sta.to_dict(),"gaze")
        mouse_sta = analysis.calc_statistic(mouse_norm)
        mouse_sta = analysis.makeArray(mouse_sta.to_dict(),"mouse")
        head_sta = analysis.calc_statistic(head_norm)
        head_sta = analysis.makeArray(head_sta.to_dict(),"head")
        
        visit_dict =  {"visit":visit_time,"visitword":visit_timeWord,"movetime":move_time,"move_word":move_word}
        #visit_dict =  {"visit":visit_time,"visitword":visit_timeWord}
        
        
        # まとめる
        dataset = gaze_sta
        dataset.update(mouse_sta)
        dataset.update(head_sta)
        dataset.update(gazehist)
        dataset.update(visit_dict)
        """
        dataset = visit_dict
        """
        #import ipdb;ipdb.set_trace()
        dataset = pd.DataFrame(pd.Series(dataset)).T
        _ankert = pd.DataFrame(_ankert).T
        
        #print(articlepath,_ankert["Interest"].values[0])
        if datasets is None:
            datasets = dataset 
            ankerts = _ankert
        else:
            datasets = pd.concat([datasets,dataset],ignore_index=False)
            ankerts = pd.concat([ankerts,_ankert],ignore_index=False)
    
    return datasets,ankerts

def fillna(_data):
    if ((np.isnan(_data)) | (_data==float("inf")) | (_data==float("-inf"))).any().any():
            #import ipdb;ipdb.set_trace()
            _data[(np.isnan(_data)) | (_data==float("inf")) | (_data==float("-inf"))] = 0.0
    return _data

def makefeatures3(articlepaths):
    """
    記事のパスリストにしたがって，
    特徴量をまとめる(時系列用)
    """
    gF = Gaze()
    mF = Mouse()
    dF = Dom()
    hF = Head()
    analysis = Analysis()
    datasets = None
    ankerts = None
    #cache = "cacheF/" 
    #cache = "/mnt/data/shobayashi/cacheF/" 
    for articlepath in articlepaths:
        """
        記事ごとに処理
        """
        #print(articlepath)
        _dir = "/".join(articlepath.split("/")[:-1])
        _id = articlepath.split("/")[-1].replace("_dom.csv","")
        if os.path.exists(cache + "cacheF/" + articlepath.replace("/","_")+ '.pickle'):
            continue
        # キャッシュを考慮してデータを読み込む
        df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert =  readdataforCache(articlepath,_dir,_id)
        #df_mouse["dtime"] = df_mouse["dtime"] - df_visit["dtime"].values[0]
        #df_head["dtime"] = df_head["dtime"] - df_visit["dtime"].values[0]
        """
        # 時間部分の正規化
        df_gaze["dtime"] = Normalization_toTime(df_gaze["dtime"],df_visit["dtime"].values)
        df_mouse["dtime"] = Normalization_toTime(df_head["dtime"],df_visit["dtime"].values)
        df_mouse["dtime"] = Normalization_toTime(df_mouse["dtime"],df_visit["dtime"].values)
        """
        #import ipdb;ipdb.set_trace()
        # 記事に対する正規化
        gaze_norm = gF.NormalizationFordom(df_gaze,df_dom)
        mouse_norm = mF.NormalizationFordom(df_mouse,df_dom)
        head_norm = hF.Normalization(df_head)
        dom_norm = dF.Normalization(df_dom)

        """
        特徴量の追加
        """
        # 差分
        gaze_norm = gF.add_speed(gaze_norm)
        head_norm = hF.add_speed(head_norm)
        # 視線のヒストグラム
        #gazehist = gF.histgram2d(df_gaze[["x","y"]])
        # 閲覧時間
        visit_time =  df_visit["dtime"].diff().values[-1] # 閲覧時間
        #visit_timeh = visit_time / dF.domSize(df_dom)
        #visit_timeWord = visit_time / dF.wordcount(df_dom) # 閲覧時間を単語数で割った
        # 文字
        #move_word = gF.calc_move(gaze_norm) / dF.wordcount(df_dom) 
        #move_time =  gF.calc_move(gaze_norm) / visit_time
        dom_norm = dF.add_features2(dom_norm)

        # 時系列データの整形
        #gaze_sta = analysis.calc_statistic(gaze_norm)
        #gaze_sta = analysis.makeArray(gaze_sta.to_dict(),"gaze")
        #mouse_sta = analysis.calc_statistic(mouse_norm)
        #mouse_sta = analysis.makeArray(mouse_sta.to_dict(),"mouse")
        #head_sta = analysis.calc_statistic(head_norm)
        #head_sta = analysis.makeArray(head_sta.to_dict(),"head")
        #visit_dict =  {"visit":visit_time,"visith":visit_timeh,"visitword":visit_timeWord,"movetime":move_time,"move_word":move_word}
        visit_dict =  {"visit":visit_time}
        # まとめる
        #temp = gazehist
        temp = visit_dict
        temp = pd.DataFrame(pd.Series(temp)).T
        dataset = [np.array(fillna(gaze_norm)).astype(np.float32)
                    ,np.array(fillna(mouse_norm)).astype(np.float32)
                    ,np.array(fillna(head_norm)).astype(np.float32)
                    ,np.array(dom_norm.drop(["element","content"],axis=1)).astype(np.float32)
                    ,np.array(fillna(temp)).astype(np.float32) ]
        _ankert = pd.DataFrame(_ankert).T
        makefeaturetoCache(articlepath,[dataset,_ankert])
        #print(articlepath,_ankert["Interest"].values[0])
        if datasets is None:
            datasets = [dataset]
            ankerts = _ankert
        else:
            datasets.append(dataset)
            ankerts = pd.concat([ankerts,_ankert],ignore_index=False)
    #import ipdb;ipdb.set_trace()
    return datasets,ankerts


def readdata(_dir,_id):
    ankert = Ankert()
    # 各データの読み込み
    df_visit,df_dom,df_gaze,df_head,df_mouse,n_page =  read_actions(_dir,_id)
    mode = df_visit["mode"].values[0]
    # アンケート結果をまとめる
    _ankert = ankert.sum_Ankert(_dir,_id,n_page,mode)
    return df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert

def readdataforCache(path,_dir,_id):
    #cache = "cache/"
    #cache = "/mnt/data/shobayashi/cache/" 
    """
    キャッシュになければ，データを整形して，読み込む
    """
    if os.path.exists(cache + "cache/" + path.replace("/","_")+ '.pickle'):
        with open(cache + "cache/"+ path.replace("/","_") + ".pickle", mode='rb') as f:
            df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert = pickle.load(f)
        return df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert
    df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert = readdata(_dir,_id)
    with open(cache+ "cache/" + path.replace("/","_") + ".pickle", mode='wb') as f:
        pickle.dump([df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert],f)
    return df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert

def makefeaturetoCache(path,data):
    """
    キャッシュになければ，データを整形して，読み込む
    """
    if not os.path.exists(cache+ "cacheF/" + path.replace("/","_")+ '.pickle'):
        with open(cache+ "cacheF/" + path.replace("/","_") + ".pickle", mode='wb') as f:
            pickle.dump(data,f)

def readfeatureforCache(path):
    with open(cache+ "cacheF/" + path.replace("/","_") + ".pickle", mode='rb') as f:
            data = pickle.load(f)
    return data

def Normalization_toTime(time,visit):
    return time / (visit[-1] - visit[0])

def makefatures(articlepaths):
    analysis =  Analysis()
    gF = Gaze()
    mF = Mouse()
    hF = Head()
    dF = Dom()
    for articlepath in articlepaths:
        """
        記事ごとに処理
        """
        #print(articlepath)
        _dir = "/".join(articlepath.split("/")[:-1])
        _id = articlepath.split("/")[-1].replace("_dom.csv","")
        
        # cacheにあればそのデータを持ってくる

        if os.path.exists(cache + "cache/" + articlepath.replace("/","_")+ '.pickle'):
            with open(cache + "cache/" + articlepath.replace("/","_") + ".pickle", mode='rb') as f:
                df_visit,df_dom,df_gaze,df_head,df_mouse,sta_gazedom,n_page = pickle.load(f)
            
        else:
            df_visit,df_dom,df_gaze,df_head,df_mouse,n_page =  read_actions(_dir,_id) #各データの読み込み
            
            
            
            # 記事の要素を正規化して返す
            df_dom_n = dF.Normalization(df_dom)

            # 記事の大きさに対する正規化
            df_gaze =  analysis.Normalization_dataFordom(df_gaze,df_dom)
            df_mouse =  analysis.Normalization_dataFordom(df_mouse,df_dom)

            # 頭部方向に関する正規化
            df_head = hF.Normalization(df_head)

            # 特徴量の追加
            #import ipdb;ipdb.set_trace()
            df_gaze = gF.add_speed(df_gaze) # 視線の速度
            df_gaze = gF.add_direction(df_gaze) # 方向ごとに速度を算出 
            #df_mouse = mF.add_speed(df_mouse)
            df_head = hF.add_speed(df_head) # 顔の動き
            df_dom = dF.add_fetures(df_dom) # 記事の特徴
            
            # 視線位置に対して，FFTの適用
            df_gaze["fx"],df_gaze["fy"] = gF.add_fft2(df_gaze[["x","y"]]) 

            # 閲覧時間より前の視線情報を削除，無駄なColumnの削除
            df_gaze = df_gaze[df_gaze["dtime"] >= df_visit["dtime"].values[0]].drop(["time","dtime"], axis=1)

            # 記事の高さに対する時間を算出
            df_gaze["dtheight"] =  analysis.Normalization_PerHeight(df_gaze["dt"],df_dom["height"])            

            # パラグラフごとに視線情報を算出し，統計量を出す
            sta_gazedom =  analysis.calc_statisticForDom(df_gaze[["x","y","dt"]],df_dom_n)

            with open(cache + "cache/" + articlepath.replace("/","_") + ".pickle", mode='wb') as f:
                pickle.dump([df_visit,df_dom,df_gaze,df_head,df_mouse,sta_gazedom,n_page],f)


        preankerts =  read_preankert(_dir)
        topic =   [True if  (int(_id) -n_page) in _pre["id"].values else False for _pre in preankerts][0] # 興味のあるトピックの記事かどうか
        
        if topic:
            #興味のあるトピックの記事
            preankerts = preankerts[0]
        else:
            #興味のないトピックの記事
            preankerts = preankerts[1]
        
        #import ipdb;ipdb.set_trace()
        #print(_id,n_page)
        preankert = preankerts[preankerts["id"]  == (int(_id) -n_page)]# 事前の印象
        
        _ankert = read_ankert(articlepath.replace("_dom","_ankert").replace(_id,str(int(_id)-n_page)))# アンケート結果
        
        # 視線のヒストグラム
        gazehist = gF.histgram2d(df_gaze[["x","y"]])

        # 統計量の計算
        sta_gaze = analysis.calc_statistic(df_gaze)
        """
        sta_mouse = analysis.calc_statistic(df_mouse)
        sta_head = analysis.calc_statistic(df_head)
        """
        
        analysis.addtodataset2("word",df_dom)
        #import ipdb;ipdb.set_trace()        
        visit_time =  df_visit["dtime"].diff().values[-1] # 閲覧時間
        visit_timeh =  visit_time / df_dom["height"] # 記事の高さで閲覧時間を割った
        visit_timeWord = visit_time / df_dom["words"] # 閲覧時間を単語数で割った
        move_time = gF.calc_move(df_gaze) / df_dom["words"]

        #analysis.addtodataset2("time",{"visit":visit_time})
        analysis.addtodataset2("time",{"visit":visit_time,"visith":visit_timeh,"visitword":visit_timeWord,"move":move_time})
        #analysis.addtodataset2("time",{"visit":visit_time,"gaze":sta_gaze["dt"]["max"],"head":sta_head["dt"]["max"],"mouse":sta_mouse["dt"]["max"]})

        analysis.addtodataset2("gazedom",sta_gazedom)
        analysis.addtodataset3("gazehist",gazehist)
        analysis.addtodataset("gaze",sta_gaze)
        #analysis.addtodataset("mouse",sta_mouse)
        #analysis.addtodataset("head",sta_head)
        
        #import ipdb;ipdb.set_trace()
        """
        analysis.addtodataset("gaze",sta_gaze.drop("dt",axis=1))
        analysis.addtodataset("mouse",sta_mouse.drop("dt",axis=1))
        analysis.addtodataset("head",sta_head.drop("dt",axis=1))
        """
        analysis.label.append([_id,preankert,_ankert])
    #import ipdb;ipdb.set_trace()
    return analysis.dataset,analysis.label

def main1():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    parser.add_argument('--label','-l',type=str, default="Interest",
                        help='比較するラベル')
    parser.add_argument('--mode','-m',type=bool, default=False,
                        help='時間制限有無')

    args = parser.parse_args()

    articlepaths =  glob.glob(args.path + "*/*/*/*_dom.csv") #収集した記事パス
    
    analysis =  Analysis()
    gF = Gaze()
    mF = Mouse()
    hF = Head()
    dF = Dom()
    for articlepath in articlepaths:
        """
        記事ごとに処理
        """
        print(articlepath)
        _dir = "/".join(articlepath.split("/")[:-1])
        _id = articlepath.split("/")[-1].replace("_dom.csv","")
        
        df_visit,df_dom,df_gaze,df_head,df_mouse,n_page =  read_actions(_dir,_id) #各データの読み込み
        
        # 時間制限のものを取り除く
        if df_visit["mode"].values[0] == args.mode:
            continue
        
        preankerts =  read_preankert(_dir)
        topic =   [True if  (int(_id) -n_page) in _pre["id"].values else False for _pre in preankerts][0] # 興味のあるトピックの記事かどうか
        
        if topic:
            #興味のあるトピックの記事
            preankerts = preankerts[0]
        else:
            #興味のないトピックの記事
            preankerts = preankerts[1]
        
        #print(_id,n_page)
        preankert = preankerts[preankerts["id"]  == (int(_id) -n_page)]# 事前の印象
        
        _ankert = read_ankert(articlepath.replace("_dom","_ankert").replace(_id,str(int(_id)-n_page)))# アンケート結果
        
        visit_time =  df_visit["dtime"].diff().values[-1]
        
        # 記事の大きさに対する正規化
        df_gaze =  analysis.Normalization_dataFordom(df_gaze,df_dom)
        df_mouse =  analysis.Normalization_dataFordom(df_mouse,df_dom)

        # 記事の要素を正規化して返す
        df_dom_n = dF.Normalization(df_dom)

        

        # 頭部方向に関する正規化
        df_head = hF.Normalization(df_head)
        
        # 特徴量の追加
        df_gaze = gF.add_speed(df_gaze) # 速度の算出
        df_gaze = gF.add_direction(df_gaze) # 方向別の速度の算出
        #df_mouse = mF.add_speed(df_mouse)
        #df_head = hF.add_speed(df_head)
        df_dom = dF.add_fetures(df_dom) # 記事に関する特徴量の算出
        df_gaze["fx"],df_gaze["fy"] = gF.add_fft2(df_gaze[["x","y"]]) # FFTの適用

        # 閲覧時間に沿って
        df_gaze = df_gaze[df_gaze["dtime"] >= df_visit["dtime"].values[0]].drop(["time","dtime"], axis=1)
        df_mouse = df_mouse[df_mouse["dtime"] >= df_visit["dtime"].values[0]].drop(["time","dtime"], axis=1)
        df_head = df_head[df_head["dtime"] >= df_visit["dtime"].values[0]].drop(["time","dtime"], axis=1)


        # 記事の大きさに対する時間の正規化
        df_gaze["dtheight"] =  analysis.Normalization_PerHeight(df_gaze["dt"],df_dom["height"])
        visit_timeh =  visit_time / df_dom["height"]
        #import ipdb;ipdb.set_trace()

        # 統計量の計算
        sta_gaze = analysis.calc_statistic(df_gaze)
        sta_gazedom =  analysis.calc_statisticForDom(df_gaze[["x","y","dt"]],df_dom_n)
        
        if ((np.isnan(sta_gaze)) | (sta_gaze==float("inf")) | (sta_gaze==float("-inf"))).any().any():
            import ipdb;ipdb.set_trace()
        
        #import ipdb;ipdb.set_trace()
        sta_mouse = analysis.calc_statistic(df_mouse)
        sta_head = analysis.calc_statistic(df_head)
        analysis.addtodataset("gaze",sta_gaze)
        analysis.addtodataset("mouse",sta_mouse)
        analysis.addtodataset("head",sta_head)
        analysis.addtodataset2("time",{"visit":visit_time,"visith":visit_timeh})
        analysis.addtodataset2("gazedom",sta_gazedom)
        analysis.label.append([_id,preankert,_ankert])
    
    #import ipdb;ipdb.set_trace()

    # 統計量の可視化
    analysis.plotfeatures("gaze",args.label)
    analysis.plotfeatures("time",args.label)
    analysis.plotfeatures("gazedom",args.label)
    #analysis.plotfeatures2(args.label)
    analysis.plotfeatures("head",args.label)
    analysis.plotfeatures("mouse",args.label)
    

class Ankert:
    def read_ankert(self,path):
        ankert = pd.read_csv(path)
        hasinterest= {
            "興味深かった":3,
            "どちらかといえば興味深かった":2,
            "どちらかといえば興味が湧かなかった":1,
            "興味が湧かなかった":0
        }
        polite= {
            "丁寧に読めた":4,
            "どちらかといえば丁寧に読めた":3,
            "どちらかといえば流し読みをした":2,
            "流し読みをした":1,
            "ほとんど読まなかった":0
        }
        Concentration = {
            "集中できた":3,
            "どちらかといえば集中できた":2,
            "どちらかといえば集中できなかった":1,
            "集中できなかった":0
        }
        know = {
            "知っていた":1,
            "知らない":0
        }
        hasknow = {
            "もっと知りたい":1,
            "もう内容について知りたいことはない":0
        }
        difficult = {
            "難しかった":2,
            "どちらとも言えない":1,
            "簡単だった":0
        }
        
        ankert.at[ankert.index[0],"Interest"] = hasinterest[ankert.at[ankert.index[0],"Interest"]]
        ankert.at[ankert.index[0],"Polite"] = polite[ankert.at[ankert.index[0],"Polite"]]
        ankert.at[ankert.index[0],"KnowInput"] = know[ankert.at[ankert.index[0],"KnowInput"]]
        ankert.at[ankert.index[0],"hasKnow"] = hasknow[ankert.at[ankert.index[0],"hasKnow"]]
        ankert.at[ankert.index[0],"Concentration"] = Concentration[ankert.at[ankert.index[0],"Concentration"]]
        ankert.at[ankert.index[0],"Difficult"] = difficult[ankert.at[ankert.index[0],"Difficult"]]
        
        return ankert

    def read_preankert(self,dir):
        _interestpath = glob.glob(dir + "/Interest*")[0] # 最初のアンケート
        _uninterestpath = glob.glob(dir + "/unInterest*")[0] #最初のアンケート
        _interest = pd.read_csv(_interestpath,names=["id","genre","pre",""],dtype={"id":np.int32,"genre":str,"pre":str})
        _uninterest = pd.read_csv(_uninterestpath,names=["id","genre","pre",""],dtype={"id":np.int32,"genre":str,"pre":str})
        know = {
            "知りたい":3,
            "どちらかといえば知りたい":2,
            "どちらかといえば知りたいと思わない":1,
            "知りたいと思わない":0
        }
        for index,_ankert in _interest.iterrows():
            _interest.at[index,"pre"] = know[_interest.at[index,"pre"][1:]] # 何故か前に空白が入っているため[1:]
        for index,_ankert in _uninterest.iterrows():
            _uninterest.at[index,"pre"] = know[_uninterest.at[index,"pre"][1:]]
        return [_interest,_uninterest]

    def sum_Ankert(self,_dir,_id,_n,mode):
        preankert = self.read_preankert(_dir)
        topic = self.checkTopic(_id,_n,preankert)
        preankert = preankert[0]  if topic else preankert[1]
        # 事前の印象
        preankert = preankert[preankert["id"]  == (int(_id) -_n)]
        # 事後の印象
        _ankert = self.read_ankert(_dir + "/" + (str(int(_id)-_n)) + "_ankert.csv")
        return self.ankert_result(_dir + "/" + str(_id),preankert,_ankert,mode)

    def checkTopic(self,_id,n_page,preankerts):
        topic =   [True if  (int(_id) -n_page) in _pre["id"].values else False for _pre in preankerts][0] # 興味のあるトピックの記事かどうか
        return topic
    
    def ankert_result(self,path,pre,after,mode):
        ankerts = pd.Series([path,pre["genre"].values[0]
                ,after["title"].values[0]
                ,pre["pre"].values[0]
                ,after['Interest'].values[0]
                ,after['Polite'].values[0]
                ,after['KnowInput'].values[0]
                ,after['hasKnow'].values[0]
                ,after['Concentration'].values[0]
                ,after['Difficult'].values[0],
                mode
                ],index=["path","genre","title","pre","Interest","Polite","Know","hasKnow","Concentration","Difficult","mode"])
        #import ipdb;ipdb.set_trace()
        return ankerts

def picktypeColumn(_type,dataset):
    temp = []
    for i in dataset.columns:
        if len(i.split(":")) < 2:
            temp.append(i)
        elif i.split(":")[1] == _type:
            temp.append(i)
    return temp 

def main2():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    parser.add_argument('--label','-l',type=str, default="Interest",
                        help='比較するラベル')
    parser.add_argument('--mode','-m',type=bool, default=False,
                        help='時間制限有無')
    parser.add_argument('--action','-a',type=str, default="gaze",
                        help='action')
    args = parser.parse_args()

    # 収集した記事のパス
    articlepaths =  glob.glob(args.path + "*/*/*/*_dom.csv") 
    articlepaths = vAnkert.checktimer("analysis/ankert/ankerts.csv",articlepaths,args.mode)
    #import ipdb;ipdb.set_trace()
    dataset, label = makefeatures2(articlepaths)
    #import ipdb; ipdb.set_trace()
    analysis = Analysis()
    analysis.plotfeatures2(dataset[picktypeColumn(args.action,dataset)],label,args.label,args.action)


if __name__ == "__main__":
    #main1()
    main2()
    pass
