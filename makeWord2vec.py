#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
#
###########################


from gensim.models import word2vec
import logging


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

sentences = word2vec.LineSentence('./wakati.txt')
model = word2vec.Word2Vec(sentences, size=200, window=15)
model.save("./wiki.model")
