#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# アンケート結果の集計
###########################

import argparse
import glob
import features as F

import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm  as cm
matplotlib.use('Agg')
labels = {
    "Interest": (
        "興味なし",
        "ほとんど興味なし",
        "少し興味深い",
        "興味深い"
    ),
    "Polite":(
        "読まなかった",
        "流し読み",
        "どちらか流し読み",
        "どちらか丁寧",
        "丁寧"
    ),
    "Concentration" :(
        "集中してない",
        "ほとんど集中してない",
        "少し集中できた",
        "集中できた"
    ),
    "Know":(
        "知らない",
        "知っていた"
    ),
    "hasKnow":(
        "知りたいことはない",
        "もっと知りたい"
    ),
    "Difficult":(
        "簡単",
        "どちらとも言えない",
        "難しい"
    ),
    "pre":(
        "知りたくない",
        "あまり知りたくない",
        "少し知りたい",
        "知りたい"
    ),
    "mode":(
        "なし",
        "時間制限"
    )
}


def ankert_result(path,pre,after,mode):
    ankerts = pd.Series([path,pre["genre"].values[0]
            ,after["title"].values[0]
            ,pre["pre"].values[0]
            ,after['Interest'].values[0]
            ,after['Polite'].values[0]
            ,after['KnowInput'].values[0]
            ,after['hasKnow'].values[0]
            ,after['Concentration'].values[0]
            ,after['Difficult'].values[0],
            mode
            ],index=["path","genre","title","pre","Interest","Polite","Know","hasKnow","Concentration","Difficult","mode"])
    #import ipdb;ipdb.set_trace()
    return ankerts

def visualize(x, y,y_label, filename="result.png"):
    df = pd.crosstab(x,y)
    #import ipdb;ipdb.set_trace()
    plt.figure()
    df.plot(kind='bar',stacked=True, legend=False,colormap=cm.winter)
    plt.legend(labels[y_label])
    plt.ylabel(y_label)
    plt.xticks(df.index , labels[df.index.name])
    plt.tight_layout()
    #plt.show()
    
    plt.savefig(filename)
    plt.close()

def visualize3d(x, y,z,y_label,z_label, filename="result.png"):
    
    plt.figure()
    w = 0.18 # 棒グラフの幅
    ind = np.arange( int(x.max())+1)# x方向の描画位置を決定するための配列
    #ax = None
    df = pd.crosstab(x,[z,y])
    #df.plot(kind='bar',stacked=True, legend=False,colormap=cm.winter)
    #plt.show()
    
    for i in df.columns.levels[0]:
        _df = df[i]
        #import ipdb;ipdb.set_trace()
        column = _df.columns.name
        index = _df.index.name
        temp = pd.DataFrame(_df,columns=range(len(labels[_df.columns.name])),index=range(len(labels[_df.index.name]))).fillna(0)
        btm = None
        #print(temp)
        btmflag = True
        #import ipdb;ipdb.set_trace()
        for _column in temp.columns:
            #import ipdb;ipdb.set_trace()
            #print(temp[_column])
            plt.bar(ind +(w*i), temp[_column], width=w, color=cm.winter(_column / len(temp.columns)), label=labels[column][int(_column)],bottom=btm,linewidth=1, edgecolor="Black") 
            if btmflag:
                btm = np.array(temp[_column].values).astype(np.int64) # 積み上げ(底上げ)のためにデータを取得
                btmflag = False    
            else:
                btm += np.array(temp[_column].values).astype(np.int64) # 積み上げ(底上げ)のためにデータを取得
            
            #plt.pause(1)
        #plt.pause(1) 
    plt.xticks(df.index + w*int(z.max())/2, labels[df.index.name])
    plt.legend(labels[y_label])
    plt.ylabel(y_label)
    plt.title(labels[z_label])
    plt.tight_layout()
    #plt.show()
    plt.savefig(filename)
    plt.close()
    #import ipdb;ipdb.set_trace()
    


def read_preankert(dir):
    _interestpath = glob.glob(dir + "/Interest*")[0] # 最初のアンケート
    _uninterestpath = glob.glob(dir + "/unInterest*")[0] #最初のアンケート
    _interest = pd.read_csv(_interestpath,names=["id","genre","pre",""],dtype={"id":np.int32,"genre":str,"pre":str})
    _uninterest = pd.read_csv(_uninterestpath,names=["id","genre","pre",""],dtype={"id":np.int32,"genre":str,"pre":str})
    
    return [_interest,_uninterest]

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    args = parser.parse_args()
    articlepaths =  glob.glob(args.path + "*/*/*/*_dom.csv") #収集した記事パス
    
    
    ankerts = pd.DataFrame([])
    for articlepath in articlepaths:
        """
        記事ごとに処理
        """
        print(articlepath)
        _dir = "/".join(articlepath.split("/")[:-1])
        _id = articlepath.split("/")[-1].replace("_dom.csv","")
        
        df_visit,_,_,_,_,n_page =  F.read_actions(_dir,_id) #各データの読み込み
       
        preankerts =  F.read_preankert(_dir)
        topic =   [True if  (int(_id) -n_page) in _pre["id"].values else False for _pre in preankerts][0] # 興味のあるトピックの記事かどうか
        
        if topic:
            #興味のあるトピックの記事
            preankerts = preankerts[0]
        else:
            #興味のないトピックの記事
            preankerts = preankerts[1]
        
        #print(_id,n_page)
        preankert = preankerts[preankerts["id"]  == (int(_id) -n_page)]# 事前の印象
        _ankert = F.read_ankert(articlepath.replace("_dom","_ankert").replace(_id,str(int(_id)-n_page)))# アンケート結果
        #import ipdb;ipdb.set_trace()
        ankerts = ankerts.append(ankert_result(articlepath,preankert,_ankert,df_visit["mode"].values[0]),ignore_index=True)

    ankerts = ankerts[["path","genre","title","pre","Interest","Polite","Know",
                        "hasKnow","Concentration","Difficult","mode"]]
    
    # 保存
    ankerts.to_csv("ankerts.csv")

    # 可視化
    """   
    visualize3d(ankerts["Interest"],ankerts["pre"],ankerts["Polite"],"pre","Polite",filename="Interest_prebyPolite.png")
    visualize3d(ankerts["pre"],ankerts["Interest"],ankerts["Polite"],"Interest","Polite",filename="pre_InterestbyPolite.png")
    visualize3d(ankerts["pre"],ankerts["Interest"],ankerts["Know"],"Interest","Know",filename="pre_InterestbyKnow.png")
    visualize3d(ankerts["pre"],ankerts["Interest"],ankerts["hasKnow"],"Interest","hasKnow",filename="pre_InterestbyhasKnow.png")
    visualize3d(ankerts["pre"],ankerts["Interest"],ankerts["Concentration"],"Interest","Concentration",filename="pre_InterestbyConcentration.png")
    visualize3d(ankerts["pre"],ankerts["Interest"],ankerts["Difficult"],"Interest","Difficult",filename="pre_InterestbyDifficult.png")
    visualize3d(ankerts["pre"],ankerts["Interest"],ankerts["mode"],"Interest","mode",filename="pre_Interestbymode.png")
    visualize3d(ankerts["Interest"],ankerts["pre"],ankerts["mode"],"pre","mode",filename="Interest_prebymode.png")
    
    visualize3d(ankerts["mode"],ankerts["Polite"],ankerts["Interest"],"Polite","Interest",filename="mode_PolitebyInterest.png")
    
    visualize(ankerts["Interest"],ankerts["pre"],"pre",filename="Interest_pre.png")
    visualize(ankerts["Interest"],ankerts["Polite"],"Polite",filename="Interest_Polite.png")
    visualize(ankerts["Interest"],ankerts["Know"],"Know",filename="Interest_Know.png")
    visualize(ankerts["Interest"],ankerts["hasKnow"],"hasKnow",filename="Interest_hasKnow.png")
    visualize(ankerts["Interest"],ankerts["Concentration"],"Concentration",filename="Interest_Concentration.png")
    visualize(ankerts["Interest"],ankerts["Difficult"],"Difficult",filename="Interest_Difficult.png")
    
    visualize(ankerts["pre"],ankerts["Interest"],"Interest",filename="pre_Interest.png")
    visualize(ankerts["pre"],ankerts["Polite"],"Polite",filename="pre_Polite.png")
    visualize(ankerts["pre"],ankerts["Know"],"Know",filename="pre_Know.png")
    visualize(ankerts["pre"],ankerts["hasKnow"],"hasKnow",filename="pre_hasKnow.png")
    visualize(ankerts["pre"],ankerts["Concentration"],"Concentration",filename="pre_Concentration.png")
    visualize(ankerts["pre"],ankerts["Difficult"],"Difficult",filename="pre_Difficult.png")

    visualize(ankerts["mode"],ankerts["Interest"],"Interest",filename="mode_Interest.png")
    visualize(ankerts["mode"],ankerts["Polite"],"Polite",filename="mode_Polite.png")
    visualize(ankerts["mode"],ankerts["Know"],"Know",filename="mode_Know.png")
    visualize(ankerts["mode"],ankerts["hasKnow"],"hasKnow",filename="mode_hasKnow.png")
    visualize(ankerts["mode"],ankerts["Concentration"],"Concentration",filename="mode_Concentration.png")
    visualize(ankerts["mode"],ankerts["Difficult"],"Difficult",filename="mode_Difficult.png")
    """


if __name__ == "__main__":
    main()
    pass
