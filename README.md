# engagement_python
- データ収集の結果（Usersをここにコピーさせて使うのがいいと思う）

## view.py
- 各種データを可視化
- [--id,-i] どの記事を
- [--path,-p] 記事があるパス
- [--output,-o] グラフ出力先

## view.sh
- view.pyを回す
- bash view.sh ディレクトリ

## features.py
- 特徴量，統計量の算出と可視化
- [--path,-p] 記事があるディレクトリ (/名前/月/日/の構造になっている)
- [--label,-l] 比較するラベル
- [--mode,-m] 時間制限有無の切り替え
- analysis/

## estimate.py
- 特徴量，統計量の算出と可視化
- [--path,-p] 記事があるディレクトリ (/名前/月/日/の構造になっている)
- [--label,-l] 比較するラベル
- [--mode,-m] 評価方法の選択
- [--result,-r] 結果の表示を行うかどうか
- [--point] 視線の密度の閾値
- [--density,-d] 密度の一覧csvのパス
## result.py
- 結果の整形
- [--result,-r] 結果のcsv
parser.add_argument('--density', '-d', type=str, default='',
                        help='density_csvファイル')
parser.add_argument('--point', type=float, default=1.0,
                        help="閾値")    

## aggregate.py
- アンケート結果の集計
- [--path,-p] 記事があるディレクトリ (/名前/月/日/の構造になっている)

## view_perAnkert.py
- ankert.csvを元にアンケート結果ごとにグラフをコピー
- [--ankert] ankert.csvのパス

## view_perResult.py
- ankert.csvを元に推定結果ごとにグラフをコピー
- [--result] 推定結果のcsvのパス

## density.py
- [--ankert] アンケートの結果一覧csv

## view_perDensity.py
- [--result] 密度のcsvのパス
- binsの位置情報を用いて，グラフをコピー