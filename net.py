#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 推定器
###########################

import argparse
import glob
import features as Features

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import result
import csv
import density
import view_perAnkert as vAnkert

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer import cuda

# Network definition
class MLP(chainer.Chain):

    def __init__(self, n_units, n_out):
        super(MLP, self).__init__()
        with self.init_scope():
            # the size of the inputs to each layer will be inferred
            self.l1 = L.Linear(None, n_units)  # n_in -> n_units
            self.l2 = L.Linear(None, n_units)  # n_units -> n_units
            self.l2_1 = L.Linear(None, n_units)  # n_units -> n_units
            self.l2_2 = L.Linear(None, n_units)  # n_units -> n_units
            self.l2_3 = L.Linear(None, n_units)  # n_units -> n_units
            self.l2_4 = L.Linear(None, n_units)  # n_units -> n_units
            self.l2_5 = L.Linear(None, n_units)  # n_units -> n_units
            self.l2_6 = L.Linear(None, n_units)  # n_units -> n_units
            self.l3 = L.Linear(None, n_out)  # n_units -> n_out
            self.bn = L.BatchNormalization(n_units)
            self.bn2 = L.BatchNormalization(n_units)
            self.bn3 = L.BatchNormalization(n_units)
            self.bn4 = L.BatchNormalization(n_units)
            self.bn5 = L.BatchNormalization(n_units)
            self.bn6 = L.BatchNormalization(n_units)
            self.bn7 = L.BatchNormalization(n_units)
            self.bn8 = L.BatchNormalization(n_units)
            self.bn9 = L.BatchNormalization(n_units)
            self.bn10 = L.BatchNormalization(n_units)
            self.bn11 = L.BatchNormalization(n_units)
            self.bn12 = L.BatchNormalization(n_units)
            self.bn13 = L.BatchNormalization(n_units)
            self.bn14 = L.BatchNormalization(n_units)
            self.bn15 = L.BatchNormalization(n_units)


    def forward(self, x):
        h1 = F.relu(self.bn(self.l1(x)))
        h2 = F.relu(self.bn2(self.l2(h1)))
        h3 = F.relu(self.bn3(self.l2_1(h2)))
        h4 = F.relu(self.bn4(self.l2_2(h3)))
        h5 = F.relu(self.bn5(self.l2_3(h4)))
        h6 = F.relu(self.bn6(self.l2_4(h5)))
        h7 = F.relu(self.bn7(self.l2_5(h6)))
        h8 = F.relu(self.bn8(self.l2_6(h7)))
        h9 = F.relu(self.bn9(self.l2(h8)))
        h10 = F.relu(self.bn10(self.l2_1(h9)))
        h11 = F.relu(self.bn11(self.l2_2(h10)))
        h12 = F.relu(self.bn12(self.l2_3(h11)))
        h13 = F.relu(self.bn13(self.l2_4(h12)))
        h14 = F.relu(self.bn14(self.l2_5(h13)))
        h15 = F.relu(self.bn15(self.l2_6(h14)))


        return  self.l3(h15)

def net(args,train,test):
    #import ipdb;ipdb.set_trace()
    # Set up a neural network to train
    # Classifier reports softmax cross entropy loss and accuracy at every
    # iteration, which will be used by the PrintReport extension below.
    model = L.Classifier(MLP(args.unit, 4))
    if args.gpu >= 0:
        # Make a specified GPU current
        chainer.backends.cuda.get_device_from_id(args.gpu).use()
        model.to_gpu()  # Copy the model to the GPU

    # Setup an optimizer
    optimizer = chainer.optimizers.SGD(lr=0.01)
    optimizer.setup(model)

    # Load the MNIST dataset
    #_train, _test = chainer.datasets.get_mnist()
    #import ipdb;ipdb.set_trace()
    train_iter = chainer.iterators.SerialIterator(train, args.batchsize)
    test_iter = chainer.iterators.SerialIterator(test, args.batchsize,
                                                 repeat=False, shuffle=False)
    # Set up a trainer
    updater = training.updaters.StandardUpdater(
        train_iter, optimizer, device=args.gpu)
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), out=args.out)

    # Evaluate the model with the test dataset for each epoch
    trainer.extend(extensions.Evaluator(test_iter, model, device=args.gpu))

    # Dump a computational graph from 'loss' variable at the first iteration
    # The "main" refers to the target link of the "main" optimizer.
    trainer.extend(extensions.dump_graph('main/loss'))

    # Take a snapshot for each specified epoch
    frequency = args.epoch if args.frequency == -1 else max(1, args.frequency)
    trainer.extend(extensions.snapshot(), trigger=(frequency, 'epoch'))

    # Write a log of evaluation statistics for each epoch
    trainer.extend(extensions.LogReport())

    # Save two plot images to the result dir
    if args.plot and extensions.PlotReport.available():
        trainer.extend(
            extensions.PlotReport(['main/loss', 'validation/main/loss'],
                                  'epoch', file_name='loss.png'))
        trainer.extend(
            extensions.PlotReport(
                ['main/accuracy', 'validation/main/accuracy'],
                'epoch', file_name='accuracy.png'))

    # Print selected entries of the log to stdout
    # Here "main" refers to the target link of the "main" optimizer again, and
    # "validation" refers to the default name of the Evaluator extension.
    # Entries other than 'epoch' are reported by the Classifier link, called by
    # either the updater or the evaluator.
    trainer.extend(extensions.PrintReport(
        ['epoch', 'main/loss', 'validation/main/loss',
         'main/accuracy', 'validation/main/accuracy', 'elapsed_time']))

    # Print a progress bar to stdout
    trainer.extend(extensions.ProgressBar())

    if args.resume:
        # Resume from a snapshot
        chainer.serializers.load_npz(args.resume, trainer)

    # Run the training
    trainer.run()

    test_iter.reset()
    #import ipdb;ipdb.set_trace()
    #for batch in  test_iter:
    batch =  chainer.dataset.concat_examples(test)                                                                      
    pred =  model.predictor(batch[0]).data.argmax(axis=1)
    return pred,batch[1]

class Estimate:
    def __init__(self,mode):
        self.mode = mode
        self.test = None
        self.train = None
        self.makedataset = {
            "dc":self.makedataset_dc,
            "dopc":self.makedataset_dopc,
            "po":self.makedataset_po
        }[self.mode]
        self.target = ""
        self.clf = RandomForestClassifier(n_estimators=100)

    def fit_randomforest(self,x,y):
        self.clf.fit(x,y)

    def predict_randomforest(self,x):
        result = self.clf.predict(x)
        return result

    def makedataset_dc(self,articles):
        self.test = articles
        self.train = articles
        yield self.train,self.test
    
    def makedataset_dopc(self,articles):
        for i,_article in enumerate(articles):
            self.target = _article
            self.test = [_article]
            self.train = [_x for _i,_x in enumerate(articles) if _i != i]
            #import ipdb;ipdb.set_trace()
            yield self.train,self.test

    def makedataset_po(self,articles):
        userdict = self.makeUserDict(articles)
        for _user in userdict.keys():
            self.target = _user
            self.test = userdict[_user]
            self.train = [ _article for _article in articles if _user not in _article]
            #import ipdb;ipdb.set_trace()
            yield self.train,self.test

    def makeUserDict(self,articles):
        users = dict()
        for _article in articles:
            _user = _article.split("/")[1]
            if _user in users.keys():
                users[_user].append(_article)
            else:
                users[_user] = [_article]
        return users

def makeInput(data):
    _input = None
    #import ipdb;ipdb.set_trace()
    for action in data.keys():   
        _data = pd.DataFrame(data[action])
        _data.columns += ("_" + action)
        #print( action,len(_data))    
        if ((np.isnan(_data)) | (_data==float("inf")) | (_data==float("-inf"))).any().any():
            #import ipdb;ipdb.set_trace()
            _data[(np.isnan(_data)) | (_data==float("inf")) | (_data==float("-inf"))] = 0.0
        
        if _input is None:
            _input = _data
        else:
            _input = pd.concat([_input,_data],axis=1)
    #import ipdb;ipdb.set_trace()
    
    return _input.fillna(0)

def makeOutput(label,target):
    labels = label[target].values.astype(int)
    #import ipdb;ipdb.set_trace()
    """
    if target == "pre":
        labels = np.array([int(l[1]["pre"]) for l in label])
    else:
        labels = np.array([int(l[2][target]) for l in label])
    """
    return labels

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    parser.add_argument('--label','-l',type=str, default="Interest",
                        help='比較するラベル')
    parser.add_argument('--mode','-m',type=str, default="dc",
                        help='評価方法の選択')
    parser.add_argument('--result','-r',type=bool, default=False,
                        help='結果の表示')
    parser.add_argument('--point',type=float, default=0.0,
                        help='視線の密度の閾値')
    parser.add_argument('--density','-d',type=str,          
                        default="analysis/density/density.csv",
                        help='密度のcsv')
    #parser = argparse.ArgumentParser(description='Chainer setting')
    parser.add_argument('--batchsize', '-b', type=int, default=5,
                        help='Number of images in each mini-batch')
    parser.add_argument('--epoch', '-e', type=int, default=20,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--frequency', '-f', type=int, default=-1,
                        help='Frequency of taking a snapshot')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')
    parser.add_argument('--resume', '-rr', default='',
                        help='Resume the training from snapshot')
    parser.add_argument('--unit', '-u', type=int, default=1024,
                        help='Number of units')
    parser.add_argument('--noplot', dest='plot', action='store_false',
                        help='Disable PlotReport extension')
    args = parser.parse_args()

    print('GPU: {}'.format(args.gpu))
    print('# unit: {}'.format(args.unit))
    print('# Minibatch-size: {}'.format(args.batchsize))
    print('# epoch: {}'.format(args.epoch))
    print('')

    articlepaths =  glob.glob(args.path + "*/*/*/*_dom.csv") #収集した記事パス
    print("総データ：",len(articlepaths))
    articlepaths = vAnkert.checktimer("analysis/ankert/ankerts.csv",articlepaths,False)
    print("対象データ：",len(articlepaths))
    articlepaths = density.checkdensity(args.density,articlepaths,args.point)
    print("見ていないデータを省く",len(articlepaths))
    estimate = Estimate(args.mode)
    #import ipdb;ipdb.set_trace()
    #estimate.makedataset(articlepaths)
    print("mode:",args.mode)
    for train,test in estimate.makedataset(articlepaths):
        print("target:",estimate.target,"train:",len(train),"test:",len(test))
        # データの準備
        x_train,y_train = Features.makefeatures2(train)
        x_test,y_test = Features.makefeatures2(test)
        #x_train = makeInput(x_train)
        x_train = x_train.fillna(0)
        #import ipdb;ipdb.set_trace()
        y_train = makeOutput(y_train,args.label)
        #x_test = makeInput(x_test)
        x_test = x_test.fillna(0)
        y_test = makeOutput(y_test,args.label)
        if (x_train.columns != x_test.columns).all():
            x_train = x_train[x_test.columns]
        _columns = x_test.columns
        if args.gpu >= 0:
            xp = cuda.cupy
        else:
            xp = np
        #import ipdb;ipdb.set_trace()
        x_train = xp.array(x_train).astype(xp.float32)
        y_train = xp.array(y_train).astype(xp.int32)
        x_test = xp.array(x_test).astype(xp.float32)
        y_test = xp.array(y_test).astype(xp.int32)

        

        # 学習と推定
        #import ipdb;ipdb.set_trace()
        pred,ans =  net(args,[(_data, _label) for _data,_label in zip(x_train,y_train)],[(_data, _label) for _data,_label in zip(x_test,y_test)])
        """
        for i,j in zip(pred,ans):
            print(i,j)
        """
        # log 
        with open(args.out + "/" +  args.mode + "_" + args.label + "_neural.csv","a") as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerows([[estimate.target,path,label,result]for path,label,result in zip(test,ans,pred)])

if __name__ == "__main__":
    main()
    pass