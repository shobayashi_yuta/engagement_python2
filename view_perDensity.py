#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 密度結果の条件に応じた記事を閲覧
###########################

import argparse
import glob
import features as F

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm  as cm
import os
import shutil
import itertools

labels = {
    "Interest": (
        "興味なし",
        "ほとんど興味なし",
        "少し興味深い",
        "興味深い"
    ),
    "Polite":(
        "読まなかった",
        "流し読み",
        "どちらか流し読み",
        "どちらか丁寧",
        "丁寧"
    ),
    "Concentration" :(
        "集中してない",
        "ほとんど集中してない",
        "少し集中できた",
        "集中できた"
    ),
    "Know":(
        "知らない",
        "知っていた"
    ),
    "hasKnow":(
        "知りたいことはない",
        "もっと知りたい"
    ),
    "Difficult":(
        "簡単",
        "どちらとも言えない",
        "難しい"
    ),
    "pre":(
        "知りたくない",
        "あまり知りたくない",
        "少し知りたい",
        "知りたい"
    ),
    "mode":(
        "なし",
        "時間制限"
    )
}



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--result',type=str, default="/",
                        help='密度csv')
    args = parser.parse_args()

    results = pd.read_csv(args.result)
    bins = pd.read_csv(args.result.replace("density.csv","bins.csv") ,names=["bin"])["bin"].values
    directory = "analysis/view_density/"
    #import ipdb;ipdb.set_trace()
    init = bins[0]
    for bin in bins[1:]:
        
        df = results[(results["density"] < bin) & (results["density"] >= init) ]
        os.makedirs(directory + str(bin) + "/")
        for _,_result in df.iterrows(): 
            shutil.copy2(_result["path"].replace("_dom.csv",".png"),directory + str(bin))
        init = bin

if __name__ == "__main__":
    main()
    pass