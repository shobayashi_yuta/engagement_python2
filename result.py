#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# ランダムフォレストの結果をまとめる
###########################

import argparse
import sys
import os
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
#import chainer 
def read_result(path):
    return pd.read_csv(path,names=["target","path","label","result"])    

def result(y,t):
    """
    y:true
    t:pred
    """
    print(confusion_matrix(y,t))
    print(classification_report(y,t))
    #import ipdb;ipdb.set_trace()


def main():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--result', '-r', type=str, default='dc.csv',
                        help='resultの結果のcsvファイル')
    parser.add_argument('--density', '-d', type=str, default='',
                        help='density_csvファイル')
    parser.add_argument('--point', type=float, default=0.0,
                        help="閾値")    
    args = parser.parse_args()

    data = read_result(args.result)
    
    if args.density != "":
        density = pd.read_csv(args.density)
        temp = pd.merge(data,density,on='path')
        #import ipdb;ipdb.set_trace()
        temp.loc[temp.density < args.point,'result'] = 0
        result(temp["label"].values,temp["result"].values)

    else:
        
        result(data["label"].values,data["result"].values)
    


if __name__ == "__main__":
    main()
    pass