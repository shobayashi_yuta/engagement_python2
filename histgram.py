#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# ヒストグラムの分布
###########################

import argparse
import glob
import view_perAnkert as vAnkert
import features as F
import numpy as np
import matplotlib.pyplot as plt

def histgram2d(_data):
    #import ipdb;ipdb.set_trace()
    hist,x,y = np.histogram2d(_data["x"].values,_data["y"].values,range=[[0.0,1.0],[0.0,1.0]],bins=[3,10],normed=True)
    return hist,x,y
    

def pickhist(articlepath):
    """
    記事のパスリストにしたがって，
    特徴量をまとめる
    """
    gF = F.Gaze()
    mF = F.Mouse()
    dF = F.Dom()
    hF = F.Head()
    analysis = F.Analysis()
    datasets = None
    ankerts = None
    
    #print(articlepath)
    _dir = "/".join(articlepath.split("/")[:-1])
    _id = articlepath.split("/")[-1].replace("_dom.csv","")
    # キャッシュを考慮してデータを読み込む
    df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert =  F.readdataforCache(articlepath,_dir,_id)

    # 記事に対する正規化
    gaze_norm = gF.NormalizationFordom(df_gaze,df_dom)
    mouse_norm = mF.NormalizationFordom(df_mouse,df_dom)
    head_norm = hF.Normalization(df_head)
    dom_norm = dF.Normalization(df_dom)
    
    """
    特徴量の追加
    """
    # 差分
    gaze_norm = gF.add_speed(gaze_norm)
    head_norm = hF.add_speed(head_norm)
    # 視線のヒストグラム
    hist,bins_x,bins_y =  histgram2d(df_gaze[["x","y"]])
    return hist,bins_x,bins_y

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    parser.add_argument('--label','-l',type=str, default="Interest",
                        help='比較するラベル')
    parser.add_argument('--mode','-m',type=bool, default=False,
                        help='時間制限有無')
    
    args = parser.parse_args()

    # 収集した記事のパス
    articlepaths =  glob.glob(args.path + "*/*/*/*_dom.csv") 
    articlepaths = vAnkert.checktimer("analysis/ankert/ankerts.csv",articlepaths,args.mode)
    #import ipdb;ipdb.set_trace()
    for articlepath in articlepaths:
        print(articlepath)
        hist,bins_x,bins_y = pickhist(articlepath)
        hist = hist.T
        fig = plt.figure(figsize=(7, 3))
        #ax = fig.add_axes()
        plt.imshow(hist, interpolation='nearest', origin='low',cmap="Blues",
            extent=[bins_x[0], bins_x[-1], bins_y[0], bins_y[-1]])
        #X, Y = np.meshgrid(bins_x, bins_y)
        #ax.pcolormesh(X, Y, hist)
        plt.gca().invert_yaxis()
        plt.savefig("analysis/histgram/" + articlepath.replace("/","_") + ".png")



if __name__ == "__main__":
    main()
    pass