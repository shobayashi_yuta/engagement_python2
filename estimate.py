#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 推定器
###########################

import argparse
import glob
import features as F

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import sklearn.ensemble
from sklearn.datasets import make_classification
import result
import csv
import density
import view_perAnkert as vAnkert
from multiprocessing import Pool

class Iterator:
    def __init__(self,mode):
        self.mode = mode
        self.test = None
        self.train = None
        self.makedataset = {
            "dc":self.makedataset_dc,
            "dopc":self.makedataset_dopc,
            "po":self.makedataset_po
        }[self.mode]
        self.target = ""
        
    def makedataset_dc(self,articles):
        self.test = articles
        self.train = articles
        yield self.train,self.test
    
    def makedataset_dopc(self,articles):
        for i,_article in enumerate(articles):
            self.target = _article
            self.test = [_article]
            self.train = [_x for _i,_x in enumerate(articles) if _i != i]
            #import ipdb;ipdb.set_trace()
            yield self.train,self.test

    def makedataset_po(self,articles):
        userdict = self.makeUserDict(articles)
        for _user in userdict.keys():
            self.target = _user
            self.test = userdict[_user]
            self.train = [ _article for _article in articles if _user not in _article]
            #import ipdb;ipdb.set_trace()
            yield self.train,self.test

    def makeUserDict(self,articles):
        users = dict()
        for _article in articles:
            _user = _article.split("/")[1]
            if _user in users.keys():
                users[_user].append(_article)
            else:
                users[_user] = [_article]
        return users

class Estimate:
    def __init__(self):
        self.clf = RandomForestClassifier(n_estimators=100,random_state=0)
        #self.clf = sklearn.ensemble.RandomForestRegressor(n_estimators=100,random_state=0)
        #self.clf = RandomForestClassifier(n_estimators=100,class_weight="balanced",random_state=0,)
        #self.clf = RandomForestClassifier(n_estimators=100,random_state=0,class_weight=classweight)

    def fit_randomforest(self,x,y):
        self.clf.fit(x,y)
    
    def predict_randomforest(self,x):
        result = self.clf.predict(x)
        return result

    

def makeInput(data):
    _input = None
    import ipdb;ipdb.set_trace()
    for action in data.keys():   
        _data = pd.DataFrame(data[action])
        _data.columns += ("_" + action)
        #print( action,len(_data))    
        if ((np.isnan(_data)) | (_data==float("inf")) | (_data==float("-inf"))).any().any():
            #import ipdb;ipdb.set_trace()
            _data[(np.isnan(_data)) | (_data==float("inf")) | (_data==float("-inf"))] = 0.0
        
        if _input is None:
            _input = _data
        else:
            _input = pd.concat([_input,_data],axis=1)
    #import ipdb;ipdb.set_trace()
    
    return _input.fillna(0)

def makeOutput(label,target):
    labels = label[target].values
    #import ipdb;ipdb.set_trace()
    labels = (labels / 2).astype(int) # 2値
    #labels = np.where(labels >1, labels-1, labels) # 3値
    return labels

def wrapper(args):
    args,target,train,test = args
    return loop(args,target,train,test)

def loop(args,target,train,test):
    estimate = Estimate()
    print("target:",target,"train:",len(train),"test:",len(test))
    # データの準備
    x_train,y_train = F.makefeatures2(train)
    #import ipdb;ipdb.set_trace()
    x_test,y_test = F.makefeatures2(test)
    #x_train = makeInput(x_train)
    x_train = x_train.fillna(0)
    #import ipdb;ipdb.set_trace()
    y_train = makeOutput(y_train,args.label)
    #x_test = makeInput(x_test)
    x_test = x_test.fillna(0)
    y_test = makeOutput(y_test,args.label)
    if (x_train.columns != x_test.columns).all():
        x_train = x_train[x_test.columns]
    _columns = x_test.columns
    x_train = np.array(x_train)
    x_test = np.array(x_test)
    x_train = np.array(x_train).astype(np.float32)
    y_train = np.array(y_train).astype(np.int32)
    x_test = np.array(x_test).astype(np.float32)
    y_test = np.array(y_test).astype(np.int32)

    # 学習と推定
    estimate.fit_randomforest(x_train,y_train)
    yy =  estimate.predict_randomforest(x_test)
    #import ipdb;ipdb.set_trace()
    # log 
    with open(args.mode + "_" + args.label + "2.csv","a") as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerows([[target,path,label,result]for path,label,result in zip(test,y_test,yy)])
    
    with open(args.mode + "_" + args.label + "_feature2.csv","a") as f:
        writer = csv.writer(f, lineterminator='\n')
        #import ipdb;ipdb.set_trace()
        writer.writerow(_columns)
        writer.writerow(estimate.clf.feature_importances_)
    
    if args.result:
        # 評価
        result.result(y_test,yy)

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--path','-p',type=str, default="/",
                        help='記事があるパス')
    parser.add_argument('--label','-l',type=str, default="Interest",
                        help='比較するラベル')
    parser.add_argument('--mode','-m',type=str, default="dc",
                        help='評価方法の選択')
    parser.add_argument('--result','-r',type=bool, default=False,
                        help='結果の表示')
    parser.add_argument('--point',type=float, default=0.0,
                        help='視線の密度の閾値')
    parser.add_argument('--density','-d',type=str,          
                        default="analysis/density/density.csv",
                        help='密度のcsv')
    args = parser.parse_args()

    articlepaths =  glob.glob(args.path + "*/*/*/*_dom.csv") #収集した記事パス
    print("総データ：",len(articlepaths))
    articlepaths = vAnkert.checktimer("analysis/ankert/ankerts.csv",articlepaths,False)
    print("対象データ：",len(articlepaths))
    articlepaths = density.checkdensity(args.density,articlepaths,args.point)
    print("見ていないデータを省く",len(articlepaths))
    iterator = Iterator(args.mode)
    #import ipdb;ipdb.set_trace()
    #estimate.makedataset(articlepaths)
    print("mode:",args.mode)

    #wrapper([[args,iterator.target,train,test] for train,test in iterator.makedataset(articlepaths)][0])
    
    p = Pool(processes=3)
    p.map(wrapper,[[args,iterator.target,train,test] for train,test in iterator.makedataset(articlepaths)])
    p.close()
    
        


if __name__ == "__main__":
    main()
    pass