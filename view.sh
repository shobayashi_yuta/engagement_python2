#!/bin/bash

#Close
#python3 train.py -d ../interestData/feature -g 0 -e 20 --gaze -o CloseG
#python3 train.py -d ../interestData/feature -g 0 -e 20 -o Close


#DO/PC
for dirpath in `find $1/*_dom.csv -type f `; do
    
    id=`echo ${dirpath//_dom.csv/} | rev | cut -d/  -f1 | rev`
    
    outdir=`echo ${dirpath//_dom.csv/} | rev | cut -d/  -f2- | rev`
    echo $dirpath
    echo ${id} $1 ${outdir}
    python3 view.py -i ${id} -p $1 -o ${outdir}
    
    
done
