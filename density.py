#! !(which python)
# encoding: utf-8
###########################
# Author: Yuta SHOBAYASHI
# 記事に対する視線の閲覧密度を算出
# 視線の点数 / 記事の範囲
###########################

import argparse
import glob
import features as F
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm  as cm
import matplotlib.mlab as mlab
import os
import shutil
import scipy.stats as stats
import features
#import ipdb;ipdb.set_trace()

labels = {
    "Interest": (
        "興味なし",
        "ほとんど興味なし",
        "少し興味深い",
        "興味深い"
    ),
    "Polite":(
        "読まなかった",
        "流し読み",
        "どちらか流し読み",
        "どちらか丁寧",
        "丁寧"
    ),
    "Concentration" :(
        "集中してない",
        "ほとんど集中してない",
        "少し集中できた",
        "集中できた"
    ),
    "Know":(
        "知らない",
        "知っていた"
    ),
    "hasKnow":(
        "知りたいことはない",
        "もっと知りたい"
    ),
    "Difficult":(
        "簡単",
        "どちらとも言えない",
        "難しい"
    ),
    "pre":(
        "知りたくない",
        "あまり知りたくない",
        "少し知りたい",
        "知りたい"
    ),
    "mode":(
        "なし",
        "時間制限"
    )
}

def iter_AnkertstoData(ankerts):
    """
    ankertsごとに各データを返す
    時間制限は省く
    visit,dom,gaze,ankert
    """
    for _,ankert in ankerts.iterrows():
        target = ankert.path.split("/")[-1].replace("_dom.csv","")
        directory = "/".join(ankert.path.split("/")[:-1]) + "/"
        #print(target,directory)
        # 各データの読み込み
        # 時間制限のものを取り除く
        if ankert.mode == 0:
            continue
        #df_visit,df_dom,df_gaze,df_head,df_mouse,n_page =  F.read_actions(directory,target)
        #import ipdb;ipdb.set_trace()
        df_visit,df_dom,df_gaze,df_head,df_mouse,_ankert =  features.readdataforCache(ankert.path,"/".join(ankert.path.split("/")[:-1]) + "/",target)    
        #df_mouse["dtime"] = df_mouse["dtime"] - df_visit["dtime"].values[0]
        #df_head["dtime"] = df_head["dtime"] - df_visit["dtime"].values[0]
        #import ipdb;ipdb.set_trace()
        yield target,directory,df_visit,df_dom,df_gaze,ankert

def calc_density(doms,gazes,time):
    Dom = F.Dom()
    moji =  Dom.wordcount(doms)
    move = calc_move(doms,gazes)
    #time = gazes["dtime"].max() - gazes["dtime"].min()
    sizes = domsize(doms)
    counts = pickdatafordom(gazes,doms).shape[0]
    #density = counts  / (sizes[0] * sizes[1])
    #density = move *time / (sizes[0] * sizes[1] * counts) 
    """
    時間 / 記事の大きさ
    """
    density = time
    #import ipdb;ipdb.set_trace()
    #density = (time * counts)  / (moji * move)
    #density_m = counts / moji
    return density

def calc_move(doms,_gazes):
    #_gazes["dt"] = _gazes["dtime"] - _gazes["dtime"].values[0]
    _gazes["dy"] = _gazes["y"].diff()# 差分
    _gazes["dx"] = _gazes["x"].diff()# 差分
    _gazes["dsy"] = np.sqrt(np.power(_gazes["dy"],2) + np.power(_gazes["dx"],2))
    _gazes = pickdatafordom(_gazes,doms)
    move = _gazes["dsy"].sum()
    return move

def domsize(doms):
    height = doms["bottom"].max() - doms["top"].min() 
    width= doms["right"].max() - doms["left"].min()
    return [height,width]

def pickdatafordom(df,doms):
    return df[(df["x"] >= doms["left"].min()) 
        & (df["x"] <= doms["right"].max()) 
        & (df["y"] >= doms["top"].min())
        & (df["y"] <= doms["bottom"].max())]

def checkdensity(path,articles,point):
    #import ipdb;ipdb.set_trace()
    temp = articles
    df = pd.read_csv(path)
    for article in articles:
        if df[df["path"] == article]["density"].values< point:
            temp.remove(article)
    return temp

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ankert',type=str, default="/",
                        help='アンケートの結果一覧csv')
    parser.add_argument('--label',type=str, default="Interest",
                        help='可視化するラベル')    
    args = parser.parse_args()

    ankerts = pd.read_csv(args.ankert)
    #columns = ankerts.columns[4:]
    density_interests = []

    for target,directory,df_visit,df_dom,df_gaze,ankert in iter_AnkertstoData(ankerts):
        print(target,directory)
        time = df_visit["dtime"].diff().values[-1]
        density = calc_density(df_dom,df_gaze,time)
        #import ipdb;ipdb.set_trace()
        density_interests.append([ankert.path,density,ankert.Interest,ankert.Polite])
    
    density_interests = pd.DataFrame(density_interests,columns=["path","density","interest",args.label])
    
    # 可視化

    plt.figure()
    #import ipdb;ipdb.set_trace()
    #result = plt.hist((tuple(density_interests[density_interests["Interest"] == 0]["density"].values),tuple(density_interests[density_interests["Interest"] == 1]["density"].values),tuple(density_interests[density_interests["Interest"] == 2]["density"].values),tuple(density_interests[density_interests["Interest"] == 3]["density"].values)),histtype="barstacked",label=("興味なし","どちらか興味なし","どちらか興味あり","興味あり"),bins=30)

    result = plt.hist([density_interests[density_interests[args.label] == i]["density"].values for i in range(len(labels[args.label]))],histtype="barstacked",label=labels[args.label] ,bins=60)

    plt.xlim((min(density_interests["density"].values), max(density_interests["density"].values)))

    
    # ガウス分布
    #param = stats.norm.fit(density_interests["density"].values)
    #x = np.linspace(min(density_interests["density"].values), max(density_interests["density"].values),100)
    #pdf_fitted = stats.norm.pdf(x,loc=param[0], scale=param[1])
    #mean = np.mean(density_interests["density"].values)
    #variance = np.var(density_interests["density"].values)
    #sigma = np.sqrt(variance)
    #dx = result[1][1] - result[1][0]
    #scale = len(density_interests["density"].values)*dx
    #plt.plot(x,pdf_fitted,"r-")
    
    #plt.show()
    plt.legend()
    plt.savefig("density.png")
    plt.close()
    density_interests.to_csv("density.csv")
    np.savetxt("bins.csv",result[1],delimiter=",")

if __name__ == "__main__":
    main()
    pass
